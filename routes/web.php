<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/about', 'HomeController@about');

Route::get('/contact', function () {
    return view('front-user.pages.contact');
});
Route::post('/contact', 'HomeController@contact');

Route::post('subscribe-newsletter', 'NewsletterController@subscribeNewsletter');
Route::get('unsubscribe-newsletter/{email}', 'NewsletterController@unsubscribeNewsletter');

Route::get('/blog', 'HomeController@blog');
Route::get('/blog-detail/{slug?}', 'HomeController@blogDetail');
Route::get('/latest-blogs', 'HomeController@getLatestBlogs');

Route::get('/faqs', 'HomeController@faqs');

Route::get('/courses', 'HomeController@courses');
Route::get('/video-channels/{id}', 'HomeController@videoChannels');

Route::get('/channel-details/{id}', 'HomeController@channelDetails');


//login/register
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login')->name('login.submit');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/register', 'Auth\RegisterController@showSignupForm')->name('register');
Route::post('/register', 'Auth\RegisterController@register')->name('register.submit');
Route::get('verify-user/{token}', 'Auth\RegisterController@verify_user');
Route::get('send-verification-mail/{email}','Auth\RegisterController@sendVerificationMail');

//forget password
Route::get('/forgot-password', function () {return view('auth.passwords.email');});
Route::post('/forgot-password', 'Auth\LoginController@forgetPassword');
Route::get('/password/reset/{token}', function ($token) {
    return view('auth.passwords.reset')->with(['token' => $token]);
});
Route::post('/password/reset/{token}', 'Auth\LoginController@updateForgotPassword');

//social login
Route::get('login/{provider?}', 'Auth\RegisterController@redirectToProvider');
Route::get('callback/{provider}', 'Auth\RegisterController@handleProviderCallback');

Route::get('/fetch-states', 'HomeController@fetchStates');
Route::get('/fetch-cities', 'HomeController@fetchCities');

//after login
Route::middleware('auth')->group(function () {

    Route::get('/change-password', function () {return view('front-user.account.change-password');});
    Route::post('/change-password', 'UserController@changePassword');

    Route::get('/my-profile', 'UserController@editProfile');
    Route::post('/my-profile', 'UserController@updateProfile');

    Route::get('/my-subscriptions', 'UserController@mySubscriptions');

    Route::get('/get-certificate', 'UserController@getCertificate');
    Route::post('/get-certificate', 'UserController@mailCertificate');

    Route::get('/free-downloads', 'UserController@freeDownloads');

    Route::get('/video-details/{id}', 'UserController@videoDetails');

    Route::get('get-subject-details/{id}', 'UserController@getSubjectDetails');
    Route::get('get-code-details/{code}', 'UserController@getCodeDetails');

    Route::get('/packages/{id}', 'UserController@packages');

    // This route is for payment initiate page
    Route::post('/agree-to-pay', 'UserController@agreeToPay');
    Route::post('/proceed-to-pay', 'PaymentController@proceedToPay');

    // for Initiate the order
    Route::post('/payment-initiate-request','PaymentController@Initiate');

    // for Payment complete
    Route::post('/payment-complete','PaymentController@Complete');


});


//admin
Route::prefix('admin')->group(function() {
	Route::get('login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::get('logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

	//forget password routes
	Route::get('forget-password', 'Auth\AdminLoginController@forgetPassword');
	Route::post('send-forget-password-link', 'Auth\AdminLoginController@sendForgetPasswordResetLink');
	Route::get('password-reset/{token}', function ($token) {
	    return view('admin.reset-password')->with(['token' => $token]);
	});
	Route::post('update-forget-password', 'Auth\AdminLoginController@updateAdminForgotPassword');

	
	Route::get('/', 'AdminController@index')->name('admin.dashboard');
	##Admin profile management Routes
    Route::get('/notifications', 'AdminController@notifications');
    Route::get('/profile', 'AdminController@profile')->name('admin.profile');
    Route::post('/update-admin-profile', 'AdminController@updateAdminProfile');
    Route::get('/change-password', 'AdminController@changePassword')->name('admin.change-password');
    Route::post('/update-admin-password', 'AdminController@updateAdminPassword');

    //Manage blogs
    Route::get('/manage-blogs', 'AdminController@manageBlogs');
    Route::get('/add-blog', 'AdminController@addBlog');
    Route::post('/add-blog', 'AdminController@saveBlog');
    Route::get('/edit-blog/{id?}', 'AdminController@editBlog');
    Route::post('/edit-blog/{id?}', 'AdminController@updateBlog');
    Route::post('/delete-blog', 'AdminController@deleteBlog');
    Route::post('/activate-deactivate-blog', 'AdminController@activateDeactivateBlog');

    //Manage faqs
    Route::get('/manage-faqs', 'AdminController@manageFaqs');
    Route::get('/add-faq', 'AdminController@addFaq');
    Route::post('/add-faq', 'AdminController@saveFaq');
    Route::get('/edit-faq/{id?}', 'AdminController@editFaq');
    Route::post('/edit-faq/{id?}', 'AdminController@updateFaq');
    Route::post('/delete-faq', 'AdminController@deleteFaq');
    Route::post('/activate-deactivate-faq', 'AdminController@activateDeactivateFaq');

    //Manage free-downloads
    Route::get('/manage-free-downloads', 'AdminController@manageFreeDownloads');
    Route::get('/add-free-download', 'AdminController@addFreeDownload');
    Route::post('/add-free-download', 'AdminController@saveFreeDownload');
    Route::get('/edit-free-download/{id?}', 'AdminController@editFreeDownload');
    Route::post('/edit-free-download/{id?}', 'AdminController@updateFreeDownload');
    Route::post('/delete-free-download', 'AdminController@deleteFreeDownload');

    //Manage discount-codes
    Route::get('/manage-discount-codes', 'AdminController@manageDiscountCodes');
    Route::get('/add-discount-code', 'AdminController@addDiscountCode');
    Route::post('/add-discount-code', 'AdminController@saveDiscountCode');
    Route::get('/edit-discount-code/{id?}', 'AdminController@editDiscountCode');
    Route::post('/edit-discount-code/{id?}', 'AdminController@updateDiscountCode');
    Route::post('/delete-discount-code', 'AdminController@deleteDiscountCode');

    //Manage pages
    Route::get('/manage-pages', 'AdminController@managePages');
    Route::get('/add-page', 'AdminController@addPage');
    Route::post('/add-page', 'AdminController@savePage');
    Route::get('/edit-page/{id?}', 'AdminController@editPage');
    Route::post('/edit-page/{id?}', 'AdminController@updatePage');
    Route::post('/delete-page', 'AdminController@deletePage');
    Route::post('/activate-deactivate-page', 'AdminController@activateDeactivatePage');

    //Manage CMS
    Route::get('/edit-brochure', 'AdminController@editBrochure');
    Route::post('/edit-brochure', 'AdminController@updateBrochure');

    //Manage Homepage
    Route::get('/edit-homepage', 'AdminController@editHomepageData');
    Route::post('/edit-homepage', 'AdminController@updateHomepageData');

    //Manage homepage-videos
    Route::get('/manage-homepage-videos', 'AdminController@manageHomepageVideos');
    Route::get('/add-homepage-video', 'AdminController@addHomepageVideo');
    Route::post('/add-homepage-video', 'AdminController@saveHomepageVideo');
    Route::get('/edit-homepage-video/{id?}', 'AdminController@editHomepageVideo');
    Route::post('/edit-homepage-video/{id?}', 'AdminController@updateHomepageVideo');
    Route::post('/delete-homepage-video', 'AdminController@deleteHomepageVideo');

    //Manage homepage-upcomings
    Route::get('/manage-homepage-upcomings', 'AdminController@manageHomepageUpcomings');
    Route::get('/add-homepage-upcoming', 'AdminController@addHomepageUpcoming');
    Route::post('/add-homepage-upcoming', 'AdminController@saveHomepageUpcoming');
    Route::get('/edit-homepage-upcoming/{id?}', 'AdminController@editHomepageUpcoming');
    Route::post('/edit-homepage-upcoming/{id?}', 'AdminController@updateHomepageUpcoming');
    Route::post('/delete-homepage-upcoming', 'AdminController@deleteHomepageUpcoming');
    Route::post('/activate-deactivate-homepage-upcoming', 'AdminController@activateDeactivateHomepageUpcoming');

    //Manage Aboutpage
    Route::get('/edit-about', 'AdminController@editAboutpageData');
    Route::post('/edit-about', 'AdminController@updateAboutpageData');

    //Manage email-contents
    Route::get('/manage-email-contents', 'AdminController@manageEmailContents');
    Route::get('/edit-email-content/{id?}', 'AdminController@editEmailContent');
    Route::post('/edit-email-content/{id?}', 'AdminController@updateEmailContent');

    //Manage users
    Route::get('/manage-users', 'AdminController@manageUsers');
    Route::get('/add-user', 'AdminController@addUser');
    Route::post('/add-user', 'AdminController@saveUser');
    Route::get('/edit-user/{id?}', 'AdminController@editUser');
    Route::post('/edit-user/{id?}', 'AdminController@updateUser');
    Route::post('/delete-user', 'AdminController@deleteUser');

    Route::get('/fetch-states', 'AdminController@fetchStates');
    Route::get('/fetch-cities', 'AdminController@fetchCities');

    //Manage branches
    Route::get('/manage-branches', 'AdminController@manageBranches');
    Route::get('/add-branch', 'AdminController@addBranch');
    Route::post('/add-branch', 'AdminController@saveBranch');
    Route::get('/edit-branch/{id?}', 'AdminController@editBranch');
    Route::post('/edit-branch/{id?}', 'AdminController@updateBranch');
    Route::post('/delete-branch', 'AdminController@deleteBranch');
    Route::post('/activate-deactivate-branch', 'AdminController@activateDeactivateBranch');

    //Manage subjects
    Route::get('/manage-subjects', 'AdminController@manageSubjects');
    Route::get('/add-subject', 'AdminController@addSubject');
    Route::post('/add-subject', 'AdminController@saveSubject');
    Route::get('/edit-subject/{id?}', 'AdminController@editSubject');
    Route::post('/edit-subject/{id?}', 'AdminController@updateSubject');
    Route::post('/delete-subject', 'AdminController@deleteSubject');
    Route::post('/activate-deactivate-subject', 'AdminController@activateDeactivateSubject');

    //Manage videos
    Route::get('/manage-videos', 'AdminController@manageVideos');
    Route::get('/add-video', 'AdminController@addVideo');
    Route::post('/add-video', 'AdminController@saveVideo');
    Route::get('/edit-video/{id?}', 'AdminController@editVideo');
    Route::post('/edit-video/{id?}', 'AdminController@updateVideo');
    Route::post('/delete-video', 'AdminController@deleteVideo');
    Route::post('/activate-deactivate-video', 'AdminController@activateDeactivateVideo');

    //Manage subscriptions
    Route::get('/manage-subscriptions/{user_id}', 'AdminController@manageSubscriptions');
    Route::get('/add-subscription/{user_id}', 'AdminController@addSubscription');
    Route::post('/add-subscription/{user_id}', 'AdminController@saveSubscription');
    Route::get('/edit-subscription/{id?}', 'AdminController@editSubscription');
    Route::post('/edit-subscription/{id?}', 'AdminController@updateSubscription');
    Route::post('/delete-subscription', 'AdminController@deleteSubscription');
    Route::post('/activate-deactivate-subscription', 'AdminController@activateDeactivateSubscription');

    //Manage enquiries
    Route::get('/manage-enquiries', 'AdminController@manageEnquiries');
    Route::get('/view-enquiry/{id?}', 'AdminController@viewEnquiry');
    Route::post('/view-enquiry/{id?}', 'AdminController@replyEnquiry');
    Route::post('/delete-enquiry', 'AdminController@deleteEnquiry');

    Route::get('/manage-emails', 'AdminController@manageEmails');
    Route::get('/send-email', 'AdminController@addEmail');
    Route::post('/send-email', 'AdminController@saveEmail');
    Route::get('/edit-email/{id?}', 'AdminController@editEmail');
    Route::post('/edit-email/{id?}', 'AdminController@updateEmail');
    Route::post('/delete-email', 'AdminController@deleteEmail');

});


//route for cms pages
Route::get('/{slug?}', 'HomeController@cms');
