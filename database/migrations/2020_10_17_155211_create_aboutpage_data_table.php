<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutpageDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aboutpage_data', function (Blueprint $table) {
            $table->id();
            $table->string('image');
            $table->text('description');
            $table->string('left_title');
            $table->text('left_description');
            $table->string('right_title');
            $table->text('right_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aboutpage_data');
    }
}
