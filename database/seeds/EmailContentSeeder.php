<?php

use Illuminate\Database\Seeder;

class EmailContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $email_contents = [
            [   'name' => 'addNewUser',
                'event' => 'It is sent to the user when Admin creates his account',
                'subject' => 'Account Created',
                'body' => 'Your account is successfully created by admin. You can login using the below link and credentials. You can change your password after logging in.'],
            [   'name' => 'adminSendPasswordLink',
                'event' => 'It is sent to the admin when he clicks on forget password',
                'subject' => 'Forget Password',
                'body' => 'It seems like you have forgot your password for dScholar. Click the link below and you\'ll be redirected to a secure site from which you can set a new password.'],
            [   'name' => 'registration',
                'event' => 'It is sent to the user when he register on the site',
                'subject' => 'Account Created',
                'body' => 'You are registered successfully on dScholar. Please verifiy your account.'],
            [   'name' => 'sendCertificate',
                'event' => 'It is sent to the user when he generate certificate',
                'subject' => 'Your Certificate has arrived! [SUBSCRIPTION ID]',
                'body' => '<p>Thank you for giving us an opportunity to train you.</p> 
                			<p>We do hope that you enjoyed the learning experience and the knowledge gained will help you in all your future endeavours. </p>
                			<p>Please find attached two certificates. </p>
                			<p>A formal certificate for you to include in your CV. </p>
                			<p>A graphic one for you to proudly share amongst your friends.</p>'],
            [   'name' => 'sendPasswordLink',
                'event' => 'It is sent to the user when he clicks on forget password',
                'subject' => 'Forget Password',
                'body' => 'It seems like you have forgot your password for dScholar. Click the link below and you\'ll be redirected to a secure site from which you can set a new password.'],
            [   'name' => 'subscriptionSuccessfull',
                'event' => 'It is sent to the user when he purchase subscription successfully',
                'subject' => 'Subscription successfull',
                'body' => 'This is to inform you that you have successfully subscribed for [SUBJECT]. Your subscription duration is [FROM DATE] to [TO DATE]'],

        ];
        DB::table('email_contents')->insert($email_contents);
    }
}
