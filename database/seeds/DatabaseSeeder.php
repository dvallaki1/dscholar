<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(CmsPagesSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(BrochureSeeder::class);
        $this->call(HomepageSeeder::class);
        $this->call(AboutpageSeeder::class);
        $this->call(HomepageVideoSeeder::class);
        $this->call(EmailContentSeeder::class);
    }
}
