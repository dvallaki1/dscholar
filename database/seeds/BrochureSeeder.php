<?php

use Illuminate\Database\Seeder;

class BrochureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brochures')->insert(['brochure'=>'brochures/brochure.pdf']);
    }
}
