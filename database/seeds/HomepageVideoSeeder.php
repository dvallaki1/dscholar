<?php

use Illuminate\Database\Seeder;

class HomepageVideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('homepage_videos')->insert(['video'=>'homepage_videos/video.mp4']);
    }
}
