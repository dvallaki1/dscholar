@extends('layouts.master_user')

@section('content')
<style>
    .error-template {padding:0 0 20px 0;text-align: center;}
    .error-actions {margin-top:15px;margin-bottom:15px;}
    .error-actions .btn { margin-right:10px; }
</style>
<main>
        <!-- Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>404 Page Not Found</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero Area End-->
        <!-- About Details Start -->
        <div class="about-details mt-3">
            <div class="container">
                <div class="error-template">
                    <img src="{{asset('public')}}/assets/img/page-not-found.png" class="mx-auto img-fluid mb-3">
                    <div class="error-details">
                        Sorry, an error has occured, Requested page not found!
                    </div>
                    <div class="error-actions">
                        <a href="{{url('/')}}" class="border-btn">Take Me Home</a>                         
                        <a href="{{url('contact')}}" class="border-btn">Contact Support </a>
                    </div>
                </div>
            </div>              
        </div>
    </div>
        <!-- About Details End -->
      
</main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
@endsection

