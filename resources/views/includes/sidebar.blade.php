<div class="blog_right_sidebar">                           
    <aside class="single_sidebar_widget post_category_widget">
        <h4 class="widget_title">My Account</h4>
        <ul class="list cat-list">
            <li class="{{ URL::to('my-profile') == url()->current() ? 'active' : ' ' }}">
                <a href="{{url('my-profile')}}" class="d-flex">
                    My Profile                                         
                </a>
            </li>
            <li class="{{ URL::to('my-subscriptions') == url()->current() ? 'active' : ' ' }}">
                <a href="{{url('my-subscriptions')}}" class="d-flex">
                   My Subscriptions    
                </a>
            </li>  
            <li class="{{ URL::to('get-certificate') == url()->current() ? 'active' : ' ' }}">
                <a href="{{url('get-certificate')}}" class="d-flex">
                   Get Certificate   
                </a>
            </li>                                   
            <li class="{{ URL::to('free-downloads') == url()->current() ? 'active' : ' ' }}">
                <a href="{{url('free-downloads')}}" class="d-flex">
                    Free Downloads
                </a>
            </li>
            @if(Auth::user()->password!='')
            <li class="{{ URL::to('change-password') == url()->current() ? 'active' : ' ' }}">
                <a href="{{url('change-password')}}" class="d-flex">
                    Change Password
                </a>
            </li>
            @endif
            <li>
                <a href="{{url('logout')}}" class="d-flex">
                    Logout  
                </a>
            </li>                                   
        </ul>
    </aside>       
</div>