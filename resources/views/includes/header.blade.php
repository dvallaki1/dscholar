<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{env('APP_NAME')}}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="icon" type="image/png" href="{{asset('public/assets')}}/img/favicon.png" />

    <!-- CSS here -->
    <link rel="stylesheet" href="{{asset('public/assets')}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('public/assets')}}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{asset('public/assets')}}/css/flaticon.css">
    <link rel="stylesheet" href="{{asset('public/assets')}}/css/slicknav.css">
    <link rel="stylesheet" href="{{asset('public/assets')}}/css/animate.min.css">
    <link rel="stylesheet" href="{{asset('public/assets')}}/css/magnific-popup.css">
    <link rel="stylesheet" href="{{asset('public/assets')}}/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="{{asset('public/assets')}}/css/themify-icons.css">
    <link rel="stylesheet" href="{{asset('public/assets')}}/css/slick.css">
    <!-- <link rel="stylesheet" href="{{asset('public/assets')}}/css/nice-select.css"> -->
    <link rel="stylesheet" href="{{asset('public/assets')}}/css/style.css">

    <script src="{{asset('public/assets')}}/js/vendor/jquery-1.12.4.min.js"></script>

</head>

<body>
    <!-- Preloader Start -->
    <div id="preloader-active">
  
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>               
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
    <header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header header-sticky">
                <div class="container">
                    <div class="menu-wrapper">
                        <!-- Logo -->
                        <div class="logo">
                            <a href="{{url('/')}}"><img src="{{asset('public/assets')}}/img/logo.png" alt=""></a>
                        </div>
                        <!-- Main-menu -->
                        <div class="main-menu d-none d-lg-block">
                            <nav>                                                
                                <ul id="navigation">  
                                   <li class="{{ URL::to('/') == url()->current() ? 'active' : ' ' }}"><a href="{{url('/')}}">Home</a></li> 
                                   <li class="{{ URL::to('courses') == url()->current() || URL::to('video-channels').'/'.collect(request()->segments())->last() == url()->current() || URL::to('channel-details').'/'.collect(request()->segments())->last() == url()->current() || URL::to('video-details').'/'.collect(request()->segments())->last() == url()->current() ? 'active' : ' ' }}"><a href="{{url('courses')}}">Courses</a></li> 
                                   <li><a href="{{url('storage/app').'/'.\App\Models\Brochure::first()->brochure}}" download="">Brochure</a></li>  
                                   <li class="{{ URL::to('blog') == url()->current() || URL::to('blog-detail').'/'.collect(request()->segments())->last() == url()->current() ? 'active' : ' ' }}"><a href="{{url('blog')}}">Blog</a></li>
                                   <li class="{{ URL::to('about') == url()->current() ? 'active' : ' ' }}"><a href="{{url('about')}}">About</a></li>                   
                                   <li class="{{ URL::to('contact') == url()->current() ? 'active' : ' ' }}"><a href="{{url('contact')}}">Contact</a></li>
                                   @if(Auth::check())
                                   <li><a href="#">My Account <i class="fa fa-chevron-down small text-muted"></i></a>
                                        <ul class="submenu">
                                            <li class="{{ URL::to('my-profile') == url()->current() ? 'active' : ' ' }}"><a href="{{url('my-profile')}}"> My Profile</a></li>
                                            <li class="{{ URL::to('my-subscriptions') == url()->current() ? 'active' : ' ' }}"><a href="{{url('my-subscriptions')}}">My Subscriptions</a></li>
                                            <li class="{{ URL::to('get-certificate') == url()->current() ? 'active' : ' ' }}"><a href="{{url('get-certificate')}}">Get Certificate</a></li>
                                            <li class="{{ URL::to('free-downloads') == url()->current() ? 'active' : ' ' }}"><a href="{{url('free-downloads')}}"> Free Downloads</a></li>
                                            @if(Auth::user()->password!='')
                                            <li class="{{ URL::to('change-password') == url()->current() ? 'active' : ' ' }}"><a href="{{url('change-password')}}"> Change Password</a></li>
                                            @endif
                                            <li><a href="{{url('logout')}}"> Logout</a></li>
                                        </ul>
                                    </li>  
                                    @else
                                    <li class="{{ URL::to('login') == url()->current() ? 'active' : ' ' }}"><a href="{{url('login')}}">Login</a></li>
                                    <li class="{{ URL::to('register') == url()->current() ? 'active' : ' ' }}"><a href="{{url('register')}}">Register</a></li>
                                    @endif                       
                                </ul>
                            </nav>
                        </div>
                        
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>