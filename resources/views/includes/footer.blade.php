<footer>
        <!-- Footer Start-->
        <div class="footer-area">
            <div class="container-fluid">

                <div class="newsletterRow">
                    <div class="container">
                        <h4 class="text-center mb-3">Subscribe Newsletter</h4>
                        <form action="{{url('subscribe-newsletter')}}" method="post" data-parsley-validate>
                            @csrf
                            <div class="row d-flex">         
                                <div class="col-md-9 col-sm-12">
                                     <div class="row d-flex justify-content-between">             
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" placeholder="Name" class="form-control" name="name" required="" data-parsley-required-message="Please enter your name.">
                                            </div>
                                        </div>
                                         
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="email" placeholder="Email" class="form-control" name="email" required="" data-parsley-required-message="Please enter your email." data-parsley-type-message="Please enter a valid email.">
                                            </div>
                                        </div>                                   
                                    </div>
                                </div>  
                                <div class="col-md-3">
                                    <div class="form-group ">
                                         <button type="submit" class="border-btn plainRow">Send</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row d-flex justify-content-between">
                   
                    <div class="col-xl-12">
                        <div class="single-footer-caption">
                            <div class="footer-tittle">                               
                                <ul>
                                    <li class="{{ URL::to('faqs') == url()->current() ? 'active' : ' ' }}"><a href="{{url('faqs')}}">FAQs</a></li>
                                    @php $pages = App\Models\CmsPage::whereStatus(1)->get(); @endphp
                                    @foreach($pages as $page)
                                    <li class="{{ URL::to($page->slug) == url()->current() ? 'active' : ' ' }}"><a href="{{url('/').'/'.$page->slug}}">{{$page->title}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                   
                </div>
                <!-- Footer bottom -->
                <div class="row align-items-center">
                    <div class="col-xl-12">
                        <div class="">
                            <!-- social -->
                            <div class="footer-social">
                                <a href="#"><i class="fab fa-twitter"></i></a>
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                                <a href="#"><i class="fab fa-youtube"></i></a>
                            </div>
                        </div>
                        <div class="text-center">
                            <p>Copyright &copy; 2020 dScholar. All rights reserved</p>                  
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-4 col-md-5">
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer End-->
    </footer>

    <!-- JS here -->

    <script src="{{asset('public/assets')}}/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Popper, Bootstrap -->
    <script src="{{asset('public/assets')}}/js/popper.min.js"></script>
    <script src="{{asset('public/assets')}}/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="{{asset('public/assets')}}/js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="{{asset('public/assets')}}/js/owl.carousel.min.js"></script>
    <script src="{{asset('public/assets')}}/js/slick.min.js"></script>

    <!-- One Page, Animated-HeadLin -->
    <script src="{{asset('public/assets')}}/js/wow.min.js"></script>
    <script src="{{asset('public/assets')}}/js/animated.headline.js"></script>
    <script src="{{asset('public/assets')}}/js/jquery.magnific-popup.js"></script>

    <!-- Scrollup, nice-select, sticky -->
    <script src="{{asset('public/assets')}}/js/jquery.scrollUp.min.js"></script>
    <!-- <script src="{{asset('public/assets')}}/js/jquery.nice-select.min.js"></script> -->
    <script src="{{asset('public/assets')}}/js/jquery.sticky.js"></script>
    
    <!-- contact js -->
    <!-- <script src="{{asset('public/assets')}}/js/contact.js"></script> -->
    <script src="{{asset('public/assets')}}/js/jquery.form.js"></script>
    <script src="{{asset('public/assets')}}/js/jquery.validate.min.js"></script>
    <script src="{{asset('public/assets')}}/js/mail-script.js"></script>
    <script src="{{asset('public/assets')}}/js/jquery.ajaxchimp.min.js"></script>
    
    <!-- Jquery Plugins, main Jquery -->  
    <script src="{{asset('public/assets')}}/js/plugins.js"></script>
    <script src="{{asset('public/assets')}}/js/main.js"></script>

    <script type="text/javascript" src="{{ URL::asset('public/assets/front-end/js/parsley.min.js') }}"></script>
    
</body>
</html>