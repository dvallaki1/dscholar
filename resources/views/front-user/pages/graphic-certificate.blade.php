<div style="background:url('{{public_path()}}/assets/img/cert-bg.png') no-repeat 0 0; width:845px; height:600px; margin:20px auto;">
  <table style="font-size:18px;" width="100%" cellpadding="0" cellspacing="0">
    <tr>  
      <td width="100%" style="text-align:center"><img src="{{public_path()}}/assets/img/logo.png" style="width:180px;  margin-top:50px" alt=""></td>
    </tr>
    
    <tr>  
      <td width="100%" style="text-align:center; color:#FFCB9B">
        Online Education | Classroom Coaching <br/>
        {{env('APP_URL')}}         
      </td>
    </tr>
    <tr>  
      <td width="100%" style="text-align:center; color:#106466; font-size:35px; font-weight:700; text-transform:uppercase;">
        Certificate of Achievement  
      </td>
    </tr>
    <tr>  
      <td width="100%" style="text-align:center; color:#333; padding:10px 0 0;">
      This certificate is awarded to  
      </td>
    </tr>
    <tr>  
      <td width="100%" style="text-align:center; color:#106466; padding:10px 0; font-size:45px; font-weight:700;">
        {{$user->first_name.' '.$user->last_name}}
      </td>
    </tr>
    
    <tr>  
      <td width="100%" style="text-align:center; color:#333; font-size:20px; font-weight:600;">
        for Learning the course of <strong style="color:#106466">"{{@$subscription->subject->subject}}"</strong><br/>
        on our e-Learning platform.
      </td>
    </tr>
    
    <tr>  
      <td width="100%" style="text-align:center; color:#333; padding:50px 100px 50px 50px; font-size:18px;">
        <table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td width="33%" style="text-align:center; color:#106466;">
              <span>Student ID</span><br/>
              <span><strong>{{'SUB-'.str_pad($subscription->id, 8, "0", STR_PAD_LEFT)}}</strong></span>
            </td>
            <td width="33%" style="text-align:center; color:#106466;">
              <span>Instructor</span><br/>
              <span><strong>{{$admin->first_name.' '.$admin->last_name}}</strong></span>
            </td>
            <td width="33%" style="text-align:center; color:#106466;">
              <span>Duration</span><br/>
              <span><strong>{{Date('M, Y', strtotime($subscription->from_date))}} - {{Date('M, Y', strtotime($subscription->to_date))}}</strong></span>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</div>