@extends('layouts.master_user')

@section('content')
<main>
    <!--? Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Blog</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--? Hero Area End-->
    <!--================Blog Area =================-->
    <section class="pb-5 pt-5">
        <div class="container">
            <div class="row">                   
                <div class="col-lg-8 mb-0"> 
                    @include('front-user.pages.blog-ajax')
                </div>
                <div class="col-lg-4">
                    @include('front-user.pages.latest-blogs')
                </div>  
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->
</main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
@endsection