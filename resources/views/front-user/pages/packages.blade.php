@extends('layouts.master_user')

@section('content')
<main>
    <!-- Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Packages</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero Area End-->
    <!--================login_part Area =================-->
    <section class="middleSection">
        <div class="container">
            
            <div class="row">      
                      
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h3 class="subtitle">Select Packages</h3>
                      <form method="post" action="{{url('agree-to-pay')}}" id="selectPackagesForm">  
                        @csrf 
                        <div class="row"> 
                          @foreach($subjects as $subject)
                          @php $is_subscribed = \App\Models\Subscription::whereUserId(Auth::id())->whereSubjectId($subject->id)->where('from_date','<=',date('Y-m-d'))->where('to_date','>=',date('Y-m-d'))->first(); @endphp
                          <div class="col-lg-4 col-md-4 col-sm-6" @if($is_subscribed) style="display: none" @endif>                            
                              <div class="ck-button">
                                 <label>
                                    <input name="subject_ids[]" class="selectPackage" type="checkbox" value="{{$subject->id}}" @if($subject->id==$selectedSubject->id) checked @endif>                           
                                    <span>{{$subject->subject}} <br/> <strong><i class="fa fa-rupee-sign"></i>{{$subject->price}} </strong></span>
                                 </label>                                    
                              </div>                              
                          </div>
                          @endforeach
                      </div>
                      <input type="hidden" name="amount" id="total" value="{{$selectedSubject->price + $selectedSubject->price*0.18}}">
                      <input type="hidden" name="discount_id" id="discount_id" value="">
                    </form> 
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <h3 class="subtitle">Payment Summary</h3>
                    <table class="table table-bordered">
                        <input type="hidden" name="price" id="price" value="{{$selectedSubject->price}}">
                        <input type="hidden" name="gst" id="gst" value="{{$selectedSubject->price*0.18}}">
                        
                        <tr>
                            <td class="titleTable">Total Amount</td>
                            <td class="text-right"><i class="fa fa-rupee-sign"></i><span id="price_span">{{$selectedSubject->price}}</span></td>
                        </tr>
                        <!-- <tr>
                            <td class="titleTable">Discount</td>
                            <td class="text-right"><i class="fa fa-rupee-sign"></i>100</td>
                        </tr> -->
                        <tr>
                            <td class="titleTable">GST (18%)</td>
                            <td class="text-right"><i class="fa fa-rupee-sign"></i><span id="gst_span">{{round($selectedSubject->price*0.18)}}</span></td>
                        </tr>
                        <tr>
                            <td class="titleTable"><strong>Net Amount</strong></td>
                            <td class="text-right"><strong><i class="fa fa-rupee-sign"></i><span id="total_span">{{round($selectedSubject->price + $selectedSubject->price*0.18)}}</span></strong></td>
                        </tr>
                        <tr id="discountRow" style="display: none">
                            <td class="titleTable" id="discountValue">Discount (10%)</td>
                            <td class="text-right"><i class="fa fa-rupee-sign"></i><span id="discount_span"></span></td>
                        </tr>
                    </table>
                    
                    <p>GSTIN: 27AEVPA2687N2ZP</p>
                    <div id="msg"></div>
                    <input type="text" class="form-control" placeholder="Enter Discount Code" name="code" id="code">
                    <button type="button" class="border-btn" id="applyCode">
                        Apply
                    </button>
                    <!-- <p><input type="checkbox" id="terms" required=""><label for="terms"> &nbsp;I accept <a href="#" data-toggle="modal" data-target="#myModalTerms">Terms & Conditions</a>.</label></p> -->
                    
                    <div class="paybleAmt mb-4">
                        Payable Amount  : <strong><i class="fa fa-rupee-sign"></i><span id="payable_span">{{round($selectedSubject->price + $selectedSubject->price*0.18)}}</span></strong>
                    </div>
                    <div class="form-group float-right">                                        
                        <button type="submit" class="border-btn float-right" onclick="$('#selectPackagesForm').submit()">
                            Make Payment
                        </button>
                    </div>
                    
                </div>
              </form>
                
            </div>
        </div>
    </section>
    <!--================login_part end =================-->
</main>

<div class="modal fade show" id="myModalTerms" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Purchase Conditions </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times"></i>
                </button>                 
                
            </div>
            <div class="modal-body">
                <p>The videos on this website www.bharatacharyaeducation.com, are the
                intellectual and creative property of The Founder and subsequent promoter(s)
                of this website.</p>

                 <p>By subscribing to a channel you are awarded VIEWING RIGHTS only!
                Any attempt to copy, download or repost the contents of this website is
                Punishable by Law.</p>

                 <p>We take the opportunity to remind you, that your personal details are verified
                and registered in our website.</p>

                 <p>Our Legal Team will be compelled to press charges against anyone trying to
                copy, download, repost the contents of this website.</p>

                 <p>Your subscription will be immediately Terminated before initiating Legal Action.
                All cases will be registered in Mumbai and will be in accordance with stringent
                Indian Copyright Laws.</p>
                <p><strong>For assistance in making payment call: +91 999999999</strong></p>
                
                 
                
            </div>
           
           
        </div>
    </div>
</div>
@endsection

@section('script_links')


@endsection

@section('script_codes')
<script type="text/javascript">
  $('.selectPackage').click(function()
  {
    var subject_id = $(this).val();
    if($(this).is(':checked'))
      var checked = 1;
    else
      var checked = 0;

    $.ajax({
        url: "{{url('get-subject-details')}}"+'/'+subject_id
    })
    .done(function(data) {
        var price = parseInt(data.subject.price);
        var gst = parseInt(parseInt(data.subject.price) * parseFloat(0.18));
        var total = parseInt(price)+parseInt(gst);

        var final_price;
        var final_gst;
        var final_total;

        if(checked==1)
        {
          final_price = parseInt($('#price').val()) + price;
          final_gst = parseInt($('#gst').val()) + gst;
          final_total = parseInt($('#total').val()) + total;

          $('#price').val(final_price);
          $('#gst').val(final_gst);
          $('#total').val(final_total);

          $('#price_span').html(final_price);
          $('#gst_span').html(final_gst);
          $('#total_span').html(final_total);
        }
        else
        {
          final_price = parseInt($('#price').val()) - price;
          final_gst = parseInt($('#gst').val()) - gst;
          final_total = parseInt($('#total').val()) - total;

          $('#price').val(final_price);
          $('#gst').val(final_gst);
          $('#total').val(final_total);

          $('#price_span').html(final_price);
          $('#gst_span').html(final_gst);
          $('#total_span').html(final_total);
        }
        
        $('#payable_span').html(final_total);

    });

  })

  $("#applyCode").click(function(){
    var code = $("#code").val();
    if(code=='')
      alert('Please enter Discount Code');
    else
    {
      $.ajax({
          url: "{{url('get-code-details')}}"+'/'+code
      })
      .done(function(data) {

          console.log(data);
          if(data.status=='error')
          {
            $('#discountRow').hide();
            $("#msg").html('<p class="parsley-required">'+data.msg+'</p>');
          }
          else
          {
            $('#discountRow').show();
            $("#msg").html('');
            
            var details = data.codeDetails;
            $("#discount_id").val(details.id);

            if(details.type=='Percent')
            {
              $('#discountValue').html('Discount ('+details.value+'%)');
              var discount = (parseInt($('#total').val())*parseInt(details.value))/100;
              $('#discount_span').html(discount);
              var total = parseInt($('#total').val()) - discount;
            }
            else
            {
              $('#discountValue').html('Discount (<i class="fa fa-rupee-sign"></i>'+details.value+')');
              $('#discount_span').html(details.value);
              var total = parseInt($('#total').val()) - parseInt(details.value);
            }
            $('#payable_span').html(total);
            $('#total').val(total);
          }

      });
    }
  })
</script>
@endsection