
@extends('layouts.master_user')

@section('content')
<main>
    <!-- Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>About Us</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero Area End-->
    <!-- About Details Start -->
    <div class="about-details section-padding30">
        <div class="container">
    <div class="row align-items-center justify-content-between">
      <div class="col-lg-6 col-md-6 col-sm-10">
                    <div class="choice-watch-img mb-40">
                        <img src="{{url('storage/app'.'/'.$about->image)}}" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="watch-details mb-40">
                        {!! $about->description !!}
                    </div>
                </div>
                
            </div>
  </div>  
  <section class="background-light">
    <div class="container">
      <div class="row">       
        
        <div class="col-lg-6">
          <div class="about-details-cap mb-50">
            <h4>{{ $about->left_title }}</h4>
            {!! $about->left_description !!}            
          </div>
        </div>
        <div class="col-lg-6">  

          <div class="about-details-cap mb-50">
            <h4>{{ $about->right_title }}</h4>
            
             {!! $about->right_description !!}
          </div>
        </div>
      </div>
    </div>
  </section>  
</div>
</div>
    <!-- About Details End -->
  
</main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
@endsection
