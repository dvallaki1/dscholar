<h4 class="widget_title mb-1">Recent Posts</h4>
@if(count($latestBlogs) > 0)
@foreach($latestBlogs as $latest)
<div class="sidebarPost">
    <div class="row">   
        
        <div class="col-lg-5">                                      
            <a href="{{url('blog-detail').'/'.$latest->slug}}"><img src="{{url('storage/app').'/'.$latest->image}}" class="imgPost img-fluid"></a>
        </div>
        <div class="col-lg-7 pl-md-0">  
            <div class="widPostInfo">
                <a href="{{url('blog-detail').'/'.$latest->slug}}"><h6>{{$latest->title}}</h6></a>  
                <p>{!! $latest->content !!}</p>
            </div>  
             
        </div>  
        <div class="sidebarDate">
            <span><i class="fa fa-user"></i> by {{$latest->author?$latest->author:'Admin'}}</span>
            <span class="float-right"><i class="fa fa-calendar"></i> {{date('F d, Y',strtotime($latest->created_at))}}</span>
        </div> 

    </div>
</div>
@endforeach
@else
No blogs found..
@endif
