@extends('layouts.master_user')

@section('content')
<main>
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Warning</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="about-details section-padding30">
        <div class="container">
        <div class="row align-items-center justify-content-between">
            <p>The videos on this website {{env('APP_URL')}}, are the
            intellectual and creative property of The Founder and subsequent promoter(s)
            of this website.</p>

             <p>By subscribing to a channel you are awarded VIEWING RIGHTS only!
            Any attempt to copy, download or repost the contents of this website is
            Punishable by Law.</p>

             <p>We take the opportunity to remind you, that your personal details are verified
            and registered in our website.</p>

             <p>Our Legal Team will be compelled to press charges against anyone trying to
            copy, download, repost the contents of this website.</p>

             <p>Your subscription will be immediately Terminated before initiating Legal Action.
            All cases will be registered in Mumbai and will be in accordance with stringent
            Indian Copyright Laws.</p>
            <p><strong>For assistance in making payment call: +91 999999999</strong></p>

            <form method="post" action="{{url('proceed-to-pay')}}">
                @csrf
                <input type="hidden" name="amount" value="{{@$formData['amount']}}">
                <input type="hidden" name="discount_id" value="{{@$formData['discount_id']}}">
                <input type="hidden" name="name" value="{{Auth::user()->first_name.' '.Auth::user()->last_name}}">
                <input type="hidden" name="email" value="{{Auth::user()->email}}">
                <input type="hidden" name="contactNumber" value="{{Auth::user()->phone}}">
                <button  value="submit" class="border-btn" id="rzp-button1">
                    I Agree
                </button>
            </form>
        </div>
      </div>
    </div>
</main>

@if(isset($response))
@include('includes.payment-page')
@endif

@endsection

@section('script_links')


@endsection

@section('script_codes')
@endsection