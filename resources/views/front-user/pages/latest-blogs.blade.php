<div class="blog_right_sidebar">                           
    <aside class="">
        <div class="postSearch">
            <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Search">
        </div>
        <div class="" id="latest-blog-ajax">
            @include('front-user.pages.latest-blog-ajax')
        </div>
    </aside>       
</div>

<script type="text/javascript">
    $(function(){
        $("#keyword").keyup(function(){
            var keyword = $(this).val(); 

            $.ajax({
                url: "{{url('latest-blogs')}}",
                data: { keyword: keyword }
            })
            .done(function(data) {
                //alert("Search Completed!");
                $("#latest-blog-ajax").html(data);
            });
        });
    });
</script>
