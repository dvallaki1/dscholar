@extends('layouts.master_user')

@section('content')
<main>
    <!-- Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Courses</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero Area End-->
    <!--================login_part Area =================-->
    <section class="middleSection">
        <div class="container">
            <div class="row align-items-center ">
                @if(count($branches) > 0)
                @foreach($branches as $branch)                    
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <a href="{{url('video-channels'.'/'.$branch->id)}}" class="channelCat">
                        <h2></h2>
                        <div class="channelCatPrice">{{$branch->branch}}</div>
                        <p>{{count($branch->subjects)}} Video Channel(s)</p>
                        <p>View Channel(s)</p>
                    </a>
                </div>
                @endforeach
                @else
                <p>No courses are available at the moment</p>
                @endif
            </div>
        </div>
    </section>
    <!--================login_part end =================-->
</main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
@endsection