
@extends('layouts.master_user')

@section('content')
<main>
    <!--? Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Contact Us</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--? Hero Area End-->
    <!-- ================ contact section start ================= -->
    <section class="contact-section">
        <div class="container">        

            <div class="row">
               
                <div class="col-lg-7">
         
                    <form class="form-contact contact_form" method="post" id="contactForm" data-parsley-validate>
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <h2 class="contact-title">Get In Touch</h2>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" placeholder="Name" class="form-control" name="name" required="" data-parsley-required-message="Please enter your name."@if(Auth::check()) value="{{Auth::user()->first_name.' '.Auth::user()->last_name}}" style="display: none" @endif>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="email" placeholder="Email" class="form-control" name="email" required="" data-parsley-required-message="Please enter your email." data-parsley-type-message="Please enter a valid email." @if(Auth::check()) value="{{Auth::user()->email}}" style="display: none" @endif>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <input type="text" placeholder="Subject" class="form-control" name="subject" required="" data-parsley-required-message="Please enter subject.">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <textarea name="message" cols="30" rows="9" placeholder="Message" class="form-control" required="" data-parsley-required-message="Please enter message."></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <button type="submit" class="border-btn">Send</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-5">
                    <div class="">
                      <h2 class="contact-title">For any assistance write us on</h2>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-home"></i></span>
                        <div class="media-body">
                            <h3>dScholar</h3>
            
                          
                        </div>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                        <div class="media-body">
                            <h3>+91-9999999999, +91-8888888888 </h3>
                            
                        </div>
                    </div>
                    <div class="media contact-info">
                        <span class="contact-info__icon"><i class="ti-email"></i></span>
                        <div class="media-body">
                            <h3>dscholar@info.com</h3>
                        </div>
                    </div>
        
                    <div class="paymentOption">
                      <h2 class="contact-title">Payment Options</h2>
                      
                      <ul>
                        <li>Paytm / UPI-BHIM / Google Pay / PhonePe Make a payment using any of the above towards my mobile number <span>+91-9999999999</span> </li>
                        <li>Bank transfer towards this account:<br/> 
                        A/c Name: <span>dScholar</span> <br/>
                        A/c No: <span>003805007099</span> <br/> 
                        IFSC: <span>ICIC0000038</span> <br/> 
                        Bank: <span>ICICI </span><br/> 
                        A/c Type: <span>Current Account</span> </li>
                        <li>International Students can make the payment on this link: <a href="https://www.paypal.me/dScholar">https://www.paypal.me/dScholar</a></li>
                      </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ================ contact section end ================= -->
</main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
@endsection
