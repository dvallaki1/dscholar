<div class="row">                   
@if(count($blogs) > 0)
@foreach($blogs as $blog)
    <div class="col-lg-12 mb-0">    
        <div class="postCol">
            <a href="{{url('blog-detail'.'/'.$blog->slug)}}">
                <h3 class="subtitle">{{$blog->title}}</h3>
                <img src="{{url('storage/app'.'/'.$blog->image)}}" class="imgPost img-fluid"> </a>                                    
            <div class="postInfo">
               
                <div class="sidebarDate">
                    <span class="float-left"><i class="fa fa-user"></i> by {{$blog->author?$blog->author:'Admin'}}</span>
                    <span class="float-right"><i class="fa fa-calendar"></i> {{date('F d, Y',strtotime($blog->created_at))}}</span>
                </div>
                {!! $blog->content !!}
                <a href="{{url('blog-detail').'/'.$blog->slug}}" class="border-btn">Read More</a>
            </div>  
            
        </div>
    </div>
@endforeach
@else
<p class="no-blogs">No blogs found..</p>
@endif
</div>

@if ($blogs->hasPages())
<div class="text-center float-right mb-5">
    <ul class="pagination">
        {{-- First Page Link --}}
        @if($blogs->currentPage() > 1)
            <li class="page-item"><a class="page-link" href="{{ $blogs->url(1) }}">&lt;&lt;</a></li>
        @else
            <li class="page-item disabled "><a class="page-link">&lt;&lt;</a></li>
        @endif
        {{-- Previous Page Link --}}
        @if ($blogs->onFirstPage())
            <li class="page-item disabled "><a class="page-link">&lt;</a></li>
        @else
            <li class="page-item"><a class="page-link" href="{{ $blogs->previousPageUrl() }}" rel="prev"><</a></li>
        @endif
        
        @foreach(range(1, $blogs->lastPage()) as $i)
            @if($i >= $blogs->currentPage() - 1 && $i <= $blogs->currentPage() + 1 || (($blogs->onFirstPage() || !$blogs->hasMorePages()) && $i >= $blogs->currentPage() - 2 && $i <= $blogs->currentPage() + 2) )
                @if ($i == $blogs->currentPage())
                    <li class="page-item active "><a class="page-link">{{ $i }}</a></li>
                @else
                    <li class="page-item"><a class="page-link" href="{{ $blogs->url($i) }}">{{ $i }}</a></li>
                @endif
            @endif
        @endforeach
        
        {{-- Next Page Link --}}
        @if ($blogs->hasMorePages())
            <li class="page-item"><a class="page-link" href="{{ $blogs->nextPageUrl() }}" rel="next">></a></li>
        @else
            <li class="page-item disabled "><a class="page-link">&gt;</a></li>
        @endif
        {{-- Last Page Link --}}
        @if($blogs->currentPage() < $blogs->lastPage())
            <li class="page-item"><a class="page-link" href="{{ $blogs->url($blogs->lastPage()) }}">&gt;&gt;</a></li>
        @else
            <li class="page-item  disabled "><a class="page-link">&gt;&gt;</a></li>
        @endif
    </ul>
</div>

@endif
