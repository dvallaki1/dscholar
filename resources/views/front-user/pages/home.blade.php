
@extends('layouts.master_user')

@section('content')
<main>
<section class="watch-area"  style="background:url('{{asset('public/assets')}}/img/pattern10.png')">
	<div class="container">
		<div class="row align-items-center"> 
			<!--? slider Area Start -->
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="watch-details mb-2">
					<h2 class="punchline">{{$homepage->title}}</h2>
					<h3 class="mb-4 subpunchline">{{$homepage->sub_title}}</h3>
					<p class="mb-3">{!! $homepage->description !!}</p>
					<div class="plainRow" style="position:relative; z-index:999;"><a href="{{url('courses')}}" class="border-btn">Watch More Videos</a></div>				
				</div>
						
				
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">	
			   <div class="" id="top">
					<video autoplay muted loop id="" class="videoHome">
						<!-- <source src="{{asset('public/assets')}}/img/video.mp4" type="video/mp4"  > -->
						<source src="{{url('storage/app').'/'.\App\Models\HomepageVideo::inRandomOrder()->first()->video}}" >
					</video>		
				</div>
			</div>
		</div>
	</div>
</section>	
	
</div>	
    <!-- slider Area End-->
   
    <!--? Watch Choice  Start-->
    <div class="watch-area section-padding30">
        <div class="container">
            <div class="row align-items-center justify-content-between">
				<div class="col-lg-6 col-md-6 col-sm-10">
                    <div class="choice-watch-img mb-40">
                        <img src="{{url('storage/app'.'/'.$homepage->about_image)}}" alt="">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="watch-details mb-40">
                        <h2>{{$homepage->about_title}}</h2>
                        
						<p>{!! $homepage->about_description !!}</p>
                     
						 <a href="{{url('about')}}" class="border-btn">Read More</a>
                    </div>
                </div>
                
            </div>
        
        </div>
    </div>
    <!-- Watch Choice  End-->
    @if(count($homepage_upcomings) > 0)
    <section class="writeSection">
		<div class="container">
			 <h2 class="homeTitle text-center">{{@$homepage->slider_section_title}}</h2>
			<!-- ================== 1-carousel bootstrap  ==================  -->
			<div id="carousel1_indicator" class="carousel slide" data-ride="carousel">
			  <div class="carousel-inner">
			  	@php $i=1; @endphp
			  	@foreach($homepage_upcomings as $upcoming)
				<div class="carousel-item {{$i==1?'active':''}}">
					<div class="row align-items-center justify-content-between">
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="choice-watch-img mb-40">
								<img src="{{url('storage/app'.'/'.$upcoming->image)}}" alt="" class="img-fluid">
							</div>
						</div>
						<div class="col-lg-6 col-md-6">
							<div class="watch-details mb-40">
								<h4>{{$upcoming->title}}</h4>
								{!! $upcoming->description !!}
							 </div>
						</div>                    
					</div>
				</div>
				@php $i++; @endphp
				@endforeach
			  </div>
			  <a class="carousel-control-prev" href="#carousel1_indicator" role="button" data-slide="prev">
				<span  aria-hidden="true"><i class="fa fa-chevron-left"></i></span>
				<span class="sr-only">Previous</span>
			  </a>
			  <a class="carousel-control-next" href="#carousel1_indicator" role="button" data-slide="next">
				<span  aria-hidden="true"><i class="fa fa-chevron-right"></i></span>
				<span class="sr-only">Next</span>
			  </a>
			</div> 
		</div>
	</section>
	@endif
   
</main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
@endsection
