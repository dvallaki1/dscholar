@extends('layouts.master_user')

@section('content')
<main>
    @if($page->banner!='')
    <div class="blog-image">
        <img src="{{url('storage/app').'/'.$page->banner}}" alt="">
    </div>
    @endif
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>{{$page->title}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="about-details section-padding30">
        <div class="container">
        <div class="row align-items-center justify-content-between">
          {!! $page->content !!}
        </div>
      </div>
    </div>
</main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
@endsection