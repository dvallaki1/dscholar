<div class="table-responsive">
    <table class="table table-bordered">
        <tr>                            
            <!-- <th style="width:80px" class="titleTable text-center">Sr No.</th> -->
            <th class="titleTable">Video Title</th>                         
            <th class="titleTable text-center">Duration</th>
        </tr>
        @if(count($videos) > 0)
        @foreach($videos as $video)      
        <tr>                            
            {{-- <td class="text-center">{{$i}}</td> --}}
            <td>
                <a class="listTitle @if(Auth::check() && $is_subscribed)  @else notSubscribed @endif" @if(Auth::check() && $is_subscribed) href="{{url('video-details'.'/'.$video->id)}}" @else href="#" data-title="{{$video->title}}" data-subtitle="{{$video->subtitle}}" data-subject="{{@$video->subject->subject}}" @endif>{{$video->title}}</a>
            </td>                           
            <td class="text-center">{{$video->duration}}</td>
        </tr>
        @endforeach
        @else
        No Videos added..
        @endif
    </table>
    @if ($videos->hasPages())
    <div class="text-center float-right mb-5">
        <ul class="pagination">
            {{-- First Page Link --}}
            @if($videos->currentPage() > 1)
                <li class="page-item"><a class="page-link" href="{{ $videos->url(1) }}">&lt;&lt;</a></li>
            @else
                <li class="page-item disabled "><a class="page-link">&lt;&lt;</a></li>
            @endif
            {{-- Previous Page Link --}}
            @if ($videos->onFirstPage())
                <li class="page-item disabled "><a class="page-link">&lt;</a></li>
            @else
                <li class="page-item"><a class="page-link" href="{{ $videos->previousPageUrl() }}" rel="prev"><</a></li>
            @endif
            
            @foreach(range(1, $videos->lastPage()) as $i)
                @if($i >= $videos->currentPage() - 1 && $i <= $videos->currentPage() + 1 || (($videos->onFirstPage() || !$videos->hasMorePages()) && $i >= $videos->currentPage() - 2 && $i <= $videos->currentPage() + 2) )
                    @if ($i == $videos->currentPage())
                        <li class="page-item active "><a class="page-link">{{ $i }}</a></li>
                    @else
                        <li class="page-item"><a class="page-link" href="{{ $videos->url($i) }}">{{ $i }}</a></li>
                    @endif
                @endif
            @endforeach
            
            {{-- Next Page Link --}}
            @if ($videos->hasMorePages())
                <li class="page-item"><a class="page-link" href="{{ $videos->nextPageUrl() }}" rel="next">></a></li>
            @else
                <li class="page-item disabled "><a class="page-link">&gt;</a></li>
            @endif
            {{-- Last Page Link --}}
            @if($videos->currentPage() < $videos->lastPage())
                <li class="page-item"><a class="page-link" href="{{ $videos->url($videos->lastPage()) }}">&gt;&gt;</a></li>
            @else
                <li class="page-item  disabled "><a class="page-link">&gt;&gt;</a></li>
            @endif
        </ul>
    </div>

    @endif

</div>


<script type="text/javascript">
    $('.pagination a').on('click', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $('#list-view').html('Loading...'); 
        $("html,body").animate({scrollTop: $("body").offset().top}, "100");
        setTimeout(function(){ 
            $.get(url, function (data) {
                //console.log(data);
                console.log('test');
                
                $('#list-view').html(data);
            });
        }, 1000);
        
    });

</script>