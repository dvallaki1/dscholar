@extends('layouts.master_user')

@section('content')
<main>
    <!-- Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>{{$video->title}}|{{@$video->subject->subject}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero Area End-->
    <!--================login_part Area =================-->
    @if(Auth::check())
    @php $is_subscribed = \App\Models\Subscription::whereUserId(Auth::id())->whereSubjectId(@$video->subject->id)->where('from_date','<=',date('Y-m-d'))->where('to_date','>=',date('Y-m-d'))->first(); @endphp
    @endif
    <section class="middleSection">
        <div class="container">
            <div class="row align-items-center ">
                @if(Auth::check() && $is_subscribed)
                <video autoplay="" muted="" preload="auto" controlslist="nodownload" playsinline="playsinline" controls="" src="{{url('storage/app').'/'.$video->path}}" height="100%" width="100%"></video>
                @else
                <p>Please <a href="{{url('packages'.'/'.@$video->subject->id)}}">Subscribe</a> to see the video.</p>
                @endif
            </div>
        </div>
    </section>
    <!--================login_part end =================-->
</main>

<script>
  var minimalUserResponseInMiliseconds = 100;
  var before = new Date().getTime();
  debugger;
  var after = new Date().getTime();
  if (after - before > minimalUserResponseInMiliseconds) { // user had to resume the script manually via opened dev tools 
    //document.getElementById('test').innerHTML = 'on';
  }
</script>
@endsection

@section('script_links')


@endsection

@section('script_codes')
<script type="text/javascript">
    document.addEventListener('contextmenu', event => event.preventDefault());
</script>
@endsection