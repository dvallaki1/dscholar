@extends('layouts.master_user')

@section('content')
<main>
    <!-- Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Channel - {{$subject->subject}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero Area End-->
    <!--================login_part Area =================-->
    <section class="middleSection">
        <div class="container">
            @if(Auth::check())
            @if($is_subscribed)
            You are already subscribed to this channel
            @else
            <a href="{{url('packages'.'/'.$subject->id)}}" class="border-btn">Subscribe</a>
            @endif
            @else
            Please <a href="{{url('login')}}">Login</a> to subscribe
            @endif
            <div class="channelPriceBox">
                <div class="row ">                    
                    <div class="col-lg-8 col-md-10 col-sm-8 col-8">
                        <p><span>Subscription Time : <strong>{{$subject->duration_in_months}} Month(s)</strong></span>                       
                        <span>Subscription Price : <strong><i class="fa fa-rupee-sign"></i>{{$subject->price}}</strong></span></p>
                    </div>
                    <div class="col-lg-4 col-md-2 col-sm-4 col-4 text-right listType">
                        <a href="javascript:void(0);" id="grid"><i class="fa fa-th fa-lg"></i></a>&nbsp;&nbsp;
                        <a href="javascript:void(0);" id="list"><i class="fa fa-list fa-lg"></i></a>
                    </div>
                </div>
            </div>
            <div class="row align-items-center " id="grid-view">  
                @if(count($subject->videos) > 0)
                @foreach($subject->videos as $video)                  
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <a class="thumbChannel @if(Auth::check() && $is_subscribed)  @else notSubscribed @endif" @if(Auth::check() && $is_subscribed) href="{{url('video-details'.'/'.$video->id)}}" @else href="javascript:void(0)" data-title="{{$video->title}}" data-subtitle="{{$video->subtitle}}" data-subject="{{@$video->subject->subject}}"  @endif>                           
                        <img src="{{url('storage/app').'/'.$video->thumbnail}}" class="img-fluid">
                        <!-- <video src="{{url('storage/app').'/'.$video->path}}"></video> -->
                        <p class="courseName">{{$video->title}}</p>
                        <p class="courseTime"><i class="fa fa-clock"></i> {{$video->duration}}</p>
                    </a>
                </div>
                @endforeach
                @else
                No Videos added..
                @endif
            </div>
            
            <div id="list-view" style="display:none">
                @include('front-user.pages.channel-details-ajax')
            </div>
        </div>
    </section>
    <!--================login_part end =================-->
</main>

<div class="modal fade show" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title">Introduction to microprocessors | 8085  </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times"></i>
                </button>                 
                
            </div>
            <div class="modal-body">
                <p id="modal-subtitle">Basic Introduction to microprocessors and micro controllers. Explains the basic processes of fetching, decoding and execution
                </p>
                
                <p>Please click on below link to subscribe to this channel.</p>
                
                <a href="{{url('packages'.'/'.$subject->id)}}" class=" border-btn">Subscribe Now</a>
                
            </div>
           
           
        </div>
    </div>
</div>

@if(!Auth::check())
@php \Session::put('subscribeSubject', $subject->id); @endphp
@endif

@endsection

@section('script_links')


@endsection

@section('script_codes')
<script>
    $(document).ready(function() {
      $('#list').click(function(){
        
          $("#grid-view").hide();
          $("#list-view").show();         
      });
      
      $('#grid').click(function(){
          $("#list-view").hide();
          $("#grid-view").show();         
      });

      $('.notSubscribed').click(function() {
        var title = $(this).data('title')
        if($(this).data('subject')!='')
            title += ' | '+$(this).data('subject');
        $('#modal-title').html(title);
        $('#modal-subtitle').html($(this).data('subtitle'));
        $('#myModal').modal('show');
      })
      
        
    });
</script>

@endsection