@extends('layouts.master_user')

@section('content')
<main>
        <!--? Hero Area Start-->
        <!-- <div class="slider-area ">
            <div class="single-slider slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>Blog Details</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!--? Hero Area End-->
        <!--================Blog Area =================-->
        <section class="pb-5 pt-5">
            <div class="container">
                <div class="row">                   
                    <div class="col-lg-8 mb-0"> 
                        <div class="row">                   
                            <div class="col-lg-12 mb-0">    
                                <div class="postCol">
                                    <a href=""><img src="{{url('storage/app').'/'.$blog->image}}" class="imgPost img-fluid">  </a>                                    
                                    <div class="postInfo">
                                        <h1>{{$blog->title}}</h1>
                                        <div class="sidebarDate">
                                            <span class="float-left"><i class="fa fa-user"></i> by {{$blog->author?$blog->author:'Admin'}}</span>
                                            <span class="float-right"><i class="fa fa-calendar"></i> {{date('F d, Y',strtotime($blog->created_at))}}</span>
                                        </div>
                                        {!! $blog->description !!}
                                    
                                    </div>  
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                    @include('front-user.pages.latest-blogs')
                </div> 
                </div>
            </div>
        </section>
        <!--================Blog Area =================-->
    </main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
@endsection