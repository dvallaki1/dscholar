@extends('layouts.master_user')

@section('content')
<main>
    <!-- Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>FAQs</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero Area End-->
    <!-- About Details Start -->
    <div class="about-details section-padding30">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-12">
                    <div class="accordion faqAccordian" id="accordionExample">
                        @php $i=1; @endphp
                        @foreach($faqs as $faq)
                        <div class="card">
                            <div class="card-header" id="heading{{$faq->id}}">
                                <h2 class="mb-0">
                                    <button type="button" class="btn-link @if($i!=1) collapsed @endif" data-toggle="collapse" data-target="#collapse{{$faq->id}}"><i class="fa fa-plus"></i> {{$faq->question}}</button>
                                </h2>
                            </div>
                            <div id="collapse{{$faq->id}}" class="collapse @if($i==1) show @endif" aria-labelledby="heading{{$faq->id}}" data-parent="#accordionExample">
                                <div class="card-body">
                                    {!! $faq->answer !!}
                                </div>
                            </div>
                        </div>
                        @php $i++; @endphp
                        @endforeach
                    </div>  
                </div>
                
            </div>
        </div>  
    </div>
    <!-- About Details End -->
  
</main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
<script>
    $(document).ready(function(){
        // Add minus fa for collapse element which is open by default
        $(".collapse.show").each(function(){
            $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus"); 
        });
        
        // Toggle plus minus fa on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
        }).on('hide.bs.collapse', function(){
            $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
        });
    });
</script>
@endsection