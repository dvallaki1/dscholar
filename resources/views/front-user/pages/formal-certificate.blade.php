<div style="border:4px solid #FFCB9B; width:90%; margin:20px 5%">
  <table style="font-size:18px;" width="100%" cellpadding="0" cellspacing="0">
    <tr>  
      <td width="100%" style="text-align:center"><img src="{{public_path()}}/assets/img/logo.png" style="width:180px;  margin-top:20px; margin-bottom:10px" alt=""></td>
    </tr>
    
    <tr>  
      <td width="100%" style="text-align:center; color:#FFCB9B; padding-bottom:10px;">
        Online Education | Classroom Coaching <br/>
        {{env('APP_URL')}}       
      </td>
    </tr>
    <tr>  
      <td width="100%" style="text-align:center; color:#106466; font-size:35px; font-weight:700; text-transform:uppercase;">
        Certificate of Achievement  
      </td>
    </tr>
    <tr>  
      <td width="100%" style="text-align:center; color:#333; padding:10px 0 0;">
        <table width="60%" cellpadding="0" cellspacing="0" style="border:1px solid #bbb; margin:0 auto;">
          <tr>
            <td width="50%" style="text-align:left; padding:10px; background:#FFCB9B; color:#000; border-right:1px solid #bbb; border-bottom:1px solid #bbb;">
              Name of Student
            </td>
            <td width="50%" style="text-align:left; padding:10px; color:#106466; border-bottom:1px solid #bbb;">
              {{$user->first_name.' '.$user->last_name}}
            </td>
          </tr>
          <tr>
            <td width="50%" style="text-align:left; padding:10px; background:#FFCB9B; color:#000; border-right:1px solid #bbb; border-bottom:1px solid #bbb;">
              Course Learnt
            </td>
            <td width="50%" style="text-align:left; padding:10px; color:#106466; border-bottom:1px solid #bbb;">
              {{@$subscription->subject->subject}}
            </td>
          </tr>
          <tr>
            <td width="50%" style="text-align:left; padding:10px; background:#FFCB9B; color:#000; border-right:1px solid #bbb; border-bottom:1px solid #bbb;">
              Starting Date
            </td>
            <td width="50%" style="text-align:left; padding:10px; color:#106466; border-bottom:1px solid #bbb;">
              {{Date('M, Y', strtotime($subscription->from_date))}}
            </td>
          </tr>
          <tr>
            <td width="50%" style="text-align:left; padding:10px; background:#FFCB9B; color:#000; border-right:1px solid #bbb; border-bottom:1px solid #bbb;">
              Student ID
            </td>
            <td width="50%" style="text-align:left; padding:10px; color:#106466; border-bottom:1px solid #bbb;">
              {{'SUB-'.str_pad($subscription->id, 8, "0", STR_PAD_LEFT)}}
            </td>
          </tr>
        </table>
      </td>
    </tr>     
    
    <tr>  
      <td width="100%" style="text-align:left; color:#000; padding:40px; font-size:18px; font-weight:600;">
        <p style="color:#000;">This certifies that the above mentioned student has learnt this course on our E-Learning Platform, {{env('APP_NAME')}}</p>
        <p style="color:#000;">This {{$subscription->subject?$subscription->subject->duration_in_months:'6'}} month course teaches extensive subject knowledge and real-world practical examples. We hope the course has helped our student to become a qualified professional.</p><br/>
        <p style="color:#000;">Best wishes!</p> <br />
        <p style="color:#000;">
          <strong>{{$admin->first_name.' '.$admin->last_name}}</strong> <br />
          Course Conductor
           <br /> 
        </p>
      </td>
    </tr>

  </table>
</div>