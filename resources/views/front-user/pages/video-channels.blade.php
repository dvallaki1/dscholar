@extends('layouts.master_user')

@section('content')
<main>
    <!-- Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Video Channels</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero Area End-->
    <!--================login_part Area =================-->
    <section class="middleSection">
        <div class="container">
            <div class="row align-items-center ">
                @if(count($subjects) > 0)
                @foreach($subjects as $subject)                    
                <div class="col-lg-3 col-md-4 col-sm-6">
                    @if(Auth::check())
                        @php $is_subscribed = \App\Models\Subscription::whereUserId(Auth::id())->whereSubjectId($subject->id)->where('from_date','<=',date('Y-m-d'))->where('to_date','>=',date('Y-m-d'))->first(); @endphp
                        @if($is_subscribed)
                            <div class="premium-tag">SUBSCRIBED</div>
                        @endif
                    @endif
                    <a href="{{url('channel-details'.'/'.$subject->id)}}" class="channelCat">
                        <h2>{{$subject->subject}}</h2>
                        <div class="channelCatPrice"><i class="fa fa-rupee-sign"></i>{{$subject->price}}</div>
                        <p>{{$subject->duration_in_months}} Month(s)</p>
                        <p>Unlimited Views</p>
                    </a>
                </div>
                @endforeach
                @else
                <p>No video channels are available at the moment</p>
                @endif
            </div>
        </div>
    </section>
    <!--================login_part end =================-->
</main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
@endsection