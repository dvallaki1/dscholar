@extends('layouts.master_user')

@section('content')
<main>
    <!--? Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>My Profile</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--? Hero Area End-->
    <!--================Blog Area =================-->
    <section class="blog_area pb-5 pt-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    @include('includes.sidebar')
                </div>
                <div class="col-lg-9 mb-0 login_part profilePart">
                    <div class="blog_left_sidebar">
                        
                        <div class="login_part_form mt-0">
                            <div class="login_part_form_iner">
                                
                                <form class="row contact_form"  method="post" data-parsley-validate>
                                    @csrf
                                    <div class="col-md-12 mb-4 p_star">
                                        <input type="text" class="form-control" placeholder="First Name" name="first_name" required="" data-parsley-required-message="Please enter first name." value="{{Auth::user()->first_name}}">
                                    </div>
                                    <div class="col-md-12 mb-4 p_star">
                                        <input type="text" class="form-control" placeholder="Last Name" name="last_name" required="" data-parsley-required-message="Please enter last name." value="{{Auth::user()->last_name}}">
                                    </div>
                                    <div class="col-md-12 mb-4 p_star">
                                        <input type="email" class="form-control" name="email" readonly="" value="{{Auth::user()->email}}" placeholder="Email">
                                    </div>
                                    <div class="col-md-12 mb-4 p_star">
                                        <input type="text" class="form-control" name="phone" placeholder="Phone" data-parsley-required-message="Please enter phone." data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" data-parsley-pattern-message="Please enter a valid phone number." data-parsley-maxlength="15" data-parsley-maxlength-message="Please enter a valid phone number." value="{{Auth::user()->phone}}">
                                    </div>                                                                          
                                    <div class="col-md-12 mb-4 p_star">
                                        <select class="form-control{{ $errors->has('country_id') ? ' is-invalid' : '' }}" name="country_id" id="selectCountry" required="" data-parsley-required-message="Please select country">
                                            <option value="">Select Country</option>
                                            @foreach($countries as $country)
                                            <option value="{{$country->id}}" @if(Auth::user()->country_id==$country->id) selected @endif>{{$country->country}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                                                    
                                    <div class="col-md-12 mb-4 p_star">
                                        <select class="form-control{{ $errors->has('state_id') ? ' is-invalid' : '' }}" name="state_id" id="selectState" required="" data-parsley-required-message="Please select state">
                                            <option value="">Select State</option>
                                            @foreach($states as $state)
                                            <option value="{{$state->id}}" @if(Auth::user()->state_id==$state->id) selected @endif>{{$state->state}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-12 mb-4 p_star">
                                        <select class="form-control{{ $errors->has('city_id') ? ' is-invalid' : '' }}" name="city_id" id="selectCity"  data-parsley-required-message="Please select city">
                                            <option value="">Select City</option>
                                            @foreach($cities as $city)
                                            <option value="{{$city->id}}" @if(Auth::user()->city_id==$city->id) selected @endif>{{$city->city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="col-md-12 form-group mt-4">
                                        <button type="submit" value="submit" class="border-btn">
                                           Update
                                        </button>                                           
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                
                
                
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->
</main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
<script>
  

    $('#selectCountry').change(function(){
        $("#selectState").empty();
        $.get("{{URL::to('fetch-states')}}", {country_id: $(this).val()}, function(res){
            // /console.log(res);
            $("#selectState").append(res);
        })
    });

    $('#selectState').change(function(){
        $("#selectCity").empty();
        $.get("{{URL::to('fetch-cities')}}", {state_id: $(this).val()}, function(res){
            // /console.log(res);
            $("#selectCity").append(res);
        })
    });

</script>
@endsection