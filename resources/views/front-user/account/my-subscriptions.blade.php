@extends('layouts.master_user')

@section('content')
<main>
    <!--? Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>My Subscriptions</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--? Hero Area End-->
    <!--================Blog Area =================-->
    <section class=" pb-5 pt-5">
        <div class="container">
            <div class="row">
                @include('includes.sidebar')
                <div class="col-lg-9 mb-0 login_part profilePart">
                    <div class="blog_left_sidebar">
                        
                        
                        <div class="login_part_form mt-0">
                            <div class="row mb-3">  
                                @if(count($subscriptions) > 0) 
                                @foreach($subscriptions as $subscription)                 
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <a href="{{url('channel-details'.'/'.@$subscription->subject->id)}}" class="channelCat">
                                        <h2>{{@$subscription->subject->subject}}</h2>
                                        <div class="channelCatPrice"><i class="fa fa-rupee-sign"></i>{{@$subscription->subject->price}}</div>
                                        <p>Start Date : {{Date('M d, Y', strtotime($subscription->from_date))}}</p>
                                        <p>Start Date : {{Date('M d, Y', strtotime($subscription->to_date))}}</p>
                                    </a>
                                </div>
                                @endforeach
                                @else
                                No subscriptions..
                                @endif
                            </div>
                            
                            @if(count($pastSubscriptions) > 0) 
                            <div class="row"> 
                                <div class="col-12"><h2 class="subtitle">My Old Subscriptions</h2></div>
                                @foreach($pastSubscriptions as $subscription)                 
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <a href="{{url('channel-details')}}" class="channelCat">
                                        <h2>{{@$subscription->subject->subject}}</h2>
                                        <div class="channelCatPrice"><i class="fa fa-rupee-sign"></i>{{@$subscription->subject->price}}</div>
                                        <p>Start Date : {{Date('M d, Y', strtotime($subscription->from_date))}}</p>
                                        <p>Start Date : {{Date('M d, Y', strtotime($subscription->to_date))}}</p>
                                    </a>
                                </div>
                                @endforeach
                            </div>
                            @endif
                        </div>
                    </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--================Blog Area =================-->
</main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
@endsection