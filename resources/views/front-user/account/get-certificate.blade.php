@extends('layouts.master_user')

@section('content')
<main>
        <!--? Hero Area Start-->
        <div class="slider-area ">
            <div class="single-slider slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>Get Certificate</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--? Hero Area End-->
        <!--================Blog Area =================-->
        <section class=" pb-5 pt-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        @include('includes.sidebar')
                    </div>
                    <div class="col-lg-9 mb-0 login_part profilePart">
                        <div class="blog_left_sidebar">

                            <div class="login_part_form mt-0">
                                @if(count($subscriptions) > 0) 
                                <form class="row contact_form" method="post" novalidate="novalidate" data-parsley-validate>
                                @csrf
                                <!-- <div class="row mb-3">                    
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <p>Please fill out the following fields CORRECTLY to generate Certificate:</p>
                                    </div>
                                    <div class="col-md-12 mb-2 p_star">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{Auth::user()->first_name.' '.Auth::user()->last_name}}" required="" data-parsley-required-message="Please enter your name">
                                        <p class="small text-muted">*Enter full name as desired on the certificate</p>
                                    </div>
                                    <div class="col-md-12 mb-2 p_star">
                                        <input type="email" class="form-control" id="email" name="email"  placeholder="Email" required="" data-parsley-required-message="Please enter email." data-parsley-type-message="Please enter a valid email." value="{{Auth::user()->email}}">
                                        <p class="small text-muted">*Enter your email id for receiving the certificate</p>
                                    </div>
                                </div> -->
                                
                                <h5 class="subTitle">Your Past and Present Subscriptions:</h5>
                                
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tr> 
                                            <th></th>                           
                                            <th class="titleTable text-center">Subscription ID</th>
                                            <th class="titleTable">Course</th>                          
                                            <th class="titleTable text-center">Duration</th>
                                        </tr>
                                        @foreach($subscriptions as $subscription)
                                        <tr>     
                                            <td><input type="checkbox" name="subscription_ids[]" value="{{$subscription->id}}" required="" checked=""></td>
                                            <td class="text-center">{{'SUB-'.str_pad($subscription->id, 8, "0", STR_PAD_LEFT)}}</td>
                                            <td>
                                                <a href="" class="listTitle">{{@$subscription->subject->subject}}</a>
                                            </td>                           
                                            <td class="text-center">{{Date('M d, Y', strtotime($subscription->from_date))}} - {{Date('M d, Y', strtotime($subscription->to_date))}}</td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                                <div class="plainRow form-group mt-4">                                          
                                    <button type="submit" class="border-btn">
                                       Generate
                                    </button>                        
                                </div>
                                </form>
                                @else
                                You can not generate certificate without subscription.
                                @endif
                            </div>
                        </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!--================Blog Area =================-->
    </main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
@endsection