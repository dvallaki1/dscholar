@extends('layouts.master_user')

@section('content')
<main>
    <!-- Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Free Downloads</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero Area End-->
    <!--================login_part Area =================-->
    <section class="blog_area pb-5 pt-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    @include('includes.sidebar')
                </div>
                <div class="col-lg-9 mb-0 login_part profilePart">
                    @if(count($freeDownloads) > 0)
                    <div class="channelPriceBox">
                        <div class="row ">                    
                            
                            <div class="col-12 text-right listType">
                                <a href="javascript:void(0);" id="grid"><i class="fa fa-th fa-lg"></i></a>&nbsp;&nbsp;
                                <a href="javascript:void(0);" id="list"><i class="fa fa-list fa-lg"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="row align-items-center " id="grid-view">  
                        @foreach($freeDownloads as $download)                  
                        <div class="col-lg-3 col-md-4 col-sm-6">
                            <div class="thumbChannel">
                                <img src="{{url('storage/app'.'/'.$download->thumbnail)}}" class="img-fluid">
                                <p class="courseName">{{$download->title}}</p>
                                <p class="courseTime"><a target="_blank" href="{{url('storage/app'.'/'.$download->file)}}" download=""><i class="fa fa-download"></i> Free Download</a></p>
                            </div>
                        </div>
                        @endforeach       
                    </div>
                    
                    <div id="list-view" style="display:none">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>                            
                                    <th style="width:80px" class="titleTable text-center">Sr No.</th>
                                    <th class="titleTable">Video Title</th>                         
                                    <th class="titleTable text-center">Download</th>
                                </tr>
                                @php $i=1; @endphp
                                @foreach($freeDownloads as $download)    
                                <tr>                            
                                    <td class="text-center">{{$i}}</td>
                                    <td class="">{{$download->title}}</td>                           
                                    <td class="text-center"><a target="_blank" href="{{url('storage/app'.'/'.$download->file)}}" download><i class="fa fa-download"></i> Free Download</a></td>
                                </tr>
                                @php $i++; @endphp
                                @endforeach 
                            </table>
                            <!-- <div class="text-center float-right mb-5">
                                <ul class="pagination">
                                  <li class="page-item"><a class="page-link" href="#">Prev</a></li>
                                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                                  <li class="page-item active"><a class="page-link" href="#">3</a></li>
                                  <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                </ul>
                            </div> -->
                        </div>
                    </div>
                    @else
                    <p>Downloads are not available at the moment</p>
                    @endif
                </div>

            </div>
        </div>
    </section>

    
  <!--   <section class="middleSection">
        <div class="container">
            <div class="channelPriceBox">
                <div class="row ">                    
                    
                    <div class="col-12 text-right listType">
                        <a href="javascript:void(0);" id="grid"><i class="fa fa-th fa-lg"></i></a>&nbsp;&nbsp;
                        <a href="javascript:void(0);" id="list"><i class="fa fa-list fa-lg"></i></a>
                    </div>
                </div>
            </div>
            <div class="row align-items-center " id="grid-view">                    
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="thumbChannel"">
                        
                        <img src="{{asset('public/assets')}}/img/channel-thumb.jpg" class="img-fluid">
                        <p class="courseName">8086 Pipelining</p>
                        <p class="courseTime"><a target="_blank" href="{{asset('public/assets')}}/img/Viva-Questions.pdf" download="{{asset('public/assets')}}/img/Viva-Questions.pdf"><i class="fa fa-download"></i> Free Download</a></p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="thumbChannel">              
                        <img src="{{asset('public/assets')}}/img/channel-thumb.jpg" class="img-fluid">
                        <p class="courseName"> Introduction to microprocessors</p>
                        <p class="courseTime"><a target="_blank" href="{{asset('public/assets')}}/img/Viva-Questions.pdf" download><i class="fa fa-download"></i> Free Download</a></p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="thumbChannel">
                        <img src="{{asset('public/assets')}}/img/channel-thumb.jpg" class="img-fluid">
                        <p class="courseName">8086 | Memory Banking</p>
                        <p class="courseTime"><a  target="_blank" href="{{asset('public/assets')}}/img/Viva-Questions.pdf" download><i class="fa fa-download"></i> Free Download</a></p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="thumbChannel">
                        <img src="{{asset('public/assets')}}/img/channel-thumb.jpg" class="img-fluid">
                        <p class="courseName">8086 Memory segmentation</p>
                        <p class="courseTime"><a target="_blank" href="{{asset('public/assets')}}/img/Viva-Questions.pdf" download><i class="fa fa-download"></i> Free Download</a></p>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="thumbChannel">
                        <img src="{{asset('public/assets')}}/img/channel-thumb.jpg" class="img-fluid">
                        <p class="courseName">8086 Memory segmentation</p>
                        <p class="courseTime"><a target="_blank" href="{{asset('public/assets')}}/img/Viva-Questions.pdf" download><i class="fa fa-download"></i> Free Download</a></p>
                    </div>
                </div>
                
                
            </div>
            
            <div id="list-view" style="display:none">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>                            
                            <th style="width:80px" class="titleTable text-center">Sr No.</th>
                            <th class="titleTable">Video Title</th>                         
                            <th class="titleTable text-center">Download</th>
                        </tr>
                        
                        <tr>                            
                            <td class="text-center">1</td>
                            <td class="">Introduction to microprocessors</td>                           
                            <td class="text-center"><a target="_blank" href="{{asset('public/assets')}}/img/Viva-Questions.pdf" download><i class="fa fa-download"></i> Free Download</a></td>
                        </tr>
                        <tr>                            
                            <td class="text-center">1</td>
                            <td class="">Introduction to microprocessors</td>                           
                            <td class="text-center"><a target="_blank" href="{{asset('public/assets')}}/img/Viva-Questions.pdf" download><i class="fa fa-download"></i> Free Download</a></td>
                        </tr>
                        <tr>                            
                            <td class="text-center">1</td>
                            <td class="">Introduction to microprocessors</td>                           
                            <td class="text-center"><a target="_blank" href="{{asset('public/assets')}}/img/Viva-Questions.pdf" download><i class="fa fa-download"></i> Free Download</a></td>
                        </tr>
                        <tr>                            
                            <td class="text-center">1</td>
                            <td class="">Introduction to microprocessors</td>                           
                            <td class="text-center"><a target="_blank" href="{{asset('public/assets')}}/img/Viva-Questions.pdf" download><i class="fa fa-download"></i> Free Download</a></td>
                        </tr>
                        <tr>                            
                            <td class="text-center">1</td>
                            <td class="">Introduction to microprocessors</td>                           
                            <td class="text-center"><a target="_blank" href="{{asset('public/assets')}}/img/Viva-Questions.pdf" download><i class="fa fa-download"></i> Free Download</a></td>
                        </tr>
                        <tr>                            
                            <td class="text-center">1</td>
                            <td class="">Introduction to microprocessors</td>                           
                            <td class="text-center"><a target="_blank" href="{{asset('public/assets')}}/img/Viva-Questions.pdf" download><i class="fa fa-download"></i> Free Download</a></td>
                        </tr>
                    </table>
                    <div class="text-center float-right mb-5">
                        <ul class="pagination">
                          <li class="page-item"><a class="page-link" href="#">Prev</a></li>
                          <li class="page-item"><a class="page-link" href="#">1</a></li>
                          <li class="page-item"><a class="page-link" href="#">2</a></li>
                          <li class="page-item active"><a class="page-link" href="#">3</a></li>
                          <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!--================login_part end =================-->
</main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
<script>
    $(document).ready(function() {
      $('#list').click(function(){
        
          $("#grid-view").hide();
          $("#list-view").show();         
      });
      
      $('#grid').click(function(){
          $("#list-view").hide();
          $("#grid-view").show();         
      });
      
        
    });
</script>
@endsection