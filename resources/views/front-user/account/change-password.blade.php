@extends('layouts.master_user')

@section('content')
<main>
    <!--? Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Change Password</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--? Hero Area End-->
    <!--================Blog Area =================-->
    <section class="blog_area pb-5 pt-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    @include('includes.sidebar')
                </div>
                <div class="col-lg-9 mb-0 login_part profilePart">
                    <div class="blog_left_sidebar">                         
                        
                        <div class="login_part_form mt-0">
                            <div class="login_part_form_iner">
                                
                                <form class="row contact_form"  method="post" data-parsley-validate>
                                    @csrf
                                    
                                    <div class="col-md-12 mb-4 p_star">
                                        <input type="password" class="form-control" placeholder="Current Password" name="current_password" required="" data-parsley-required-message="Please enter current password">
                                    </div>
                                    
                                    <div class="col-md-12 mb-4 p_star">
                                        <input type="password" class="form-control" placeholder="New Password" name="password" id="password" required="" data-parsley-required-message="Please enter new password">
                                    </div>
                                    
                                    <div class="col-md-12 mb-4 p_star">
                                        <input type="password" class="form-control" placeholder="Re-Enter New Password" name="password_confirmation" required="" data-parsley-required-message="Please enter current password" data-parsley-equalto="#password" data-parsley-equalto-message="New password and confirm password must be equal" >
                                    </div>
                                   
                                    <div class="col-md-12 mt-4 mt-4">
                                        
                                        <button type="submit" value="submit" class="border-btn">
                                           Change Password
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->
</main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
@endsection