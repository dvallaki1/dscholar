@include('includes.header')

@include('includes.flash-message')

@yield('content')

@include('includes.footer')

@yield('script_links')

@yield('script_codes')

