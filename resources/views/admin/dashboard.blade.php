@extends('admin.layouts.master_admin')

@section('page_title')
    {{config('app.name')}} | Dashboard
@endsection

@section('content')


    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row mt-2">
                
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3>{{$count->users}}</h3>

                            <p>Total Users</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="{{url('admin/manage-users')}}" class="small-box-footer">View <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->

            </div>

            <div class="row mt-2">
                <div class="col-lg-6">
                    <h5>Recently Registered Users</h5>
                    <div class="table-responsive">
                        <table class="table" id="recentUsers">
                            <thead>
                            <tr>
                                <th class="column-title">Created At </th>
                                <th class="column-title">Name </th>
                                <th class="column-title">Email </th>
                                <th class="column-title">Verified</th>
                                <th class="column-title">Registered Via </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1;  ?>
                            @foreach($recentUsers as $row)
                                <tr>
                                    <td>{{ $row->created_at }}</td>
                                    <td>{{ $row->first_name.' '.$row->last_name }}</td>
                                    <td>
                                        {{ $row->email }}
                                    </td>
                                    <td> @if ($row->email_verified_at != null)
                                            <span class="badge badge-success">Yes</span>
                                        @else
                                            <span class="badge badge-danger">No</span>
                                        @endif
                                    </td>
                                    <td>{{ $row->provider?ucwords($row->provider):'Email' }}</td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                            
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-6">
                    <h5>Recently Subscribed Users</h5>
                    <div class="table-responsive">
                        <table class="table" id="recentSubscriptions">
                            <thead>
                            <tr>
                                <th class="column-title">Created At </th>
                                <th class="column-title">Name </th>
                                <th class="column-title">Subject </th>
                                <th class="column-title">From Date</th>
                                <th class="column-title">To Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1;  ?>
                            @foreach($recentSubscriptions as $row)
                                <tr>
                                    <td>{{ $row->created_at }}</td>
                                    <td>{{ @$row->user->first_name.' '.@$row->user->last_name }}</td>
                                    <td>
                                        {{ @$row->subject->subject }}
                                    </td>
                                    <td>{{ date('d-m-Y',strtotime($row->from_date)) }}</td>
                                    <td>{{ date('d-m-Y',strtotime($row->to_date)) }}</td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            

            <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
    </section>

@endsection

@section('admin_script_links')  
@endsection
@section('admin_script_codes')
<script>
    $(document).ready(function() {
        $('#recentUsers').DataTable(
        {
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ]
        });
    } );

    $(document).ready(function() {
        $('#recentSubscriptions').DataTable(
        {
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ]
        });
    } );
</script>
@endsection

