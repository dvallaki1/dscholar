@extends('admin.layouts.master_admin')

@section('page_title')
{{config('app.name')}} | View Enquiry
@endsection

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>View Enquiry</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    
    <!--Begin Content-->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    
                </div>
            </div>
            <div class="card-body">
                
                <div class="form-group">
                    <label for="exampleInputEmail4">Name: </label>
                    <label>{{$row->name}}</label>
                </div>
                
                <div class="form-group">
                    <label for="exampleInputEmail4">Email: </label>
                    <label>{{$row->email}}</label>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail4">Subject: </label>
                    <label>{{$row->subject}}</label>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail4">Message: </label>
                    <label>{{$row->message}}</label>
                </div>

                @if($row->is_replied===0)
                <a class="btn btn-danger" onclick="$('#replyForm').show();$(this).hide();">Reply</a>
                @endif

                <form action="" method="post" id="replyForm" style="display: none;" enctype="multipart/form-data" data-parsley-validate>
                    @csrf
                    <fieldset>
                        <!-- <input type="hidden" name="subject" value="{{$row->subject}}">
                        <input type="hidden" name="message" value="{{$row->message}}">
                        <input type="hidden" name="email" value="{{$row->email}}">
                        <input type="hidden" name="name" value="{{$row->name}}"> -->

                        <div class="form-group ns-des show-maze-icons">
                            <label for="exampleInputEmail4">Reply <span class="text-danger">*</span></label> 
                            <textarea name="reply" id="reply" placeholder="Enter reply" class="form-control" required="" data-parsley-required-message="Please enter reply." data-parsley-errors-container="#replyError"></textarea>

                            <div id="replyError" class=""></div>
                                
                            @if ($errors->has('reply'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('reply') }}</strong>
                                </span>
                                @endif
                        </div>

                        <input type="submit" class="btn-info btn" value="Send Reply">

                    </fieldset>
                </form>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                {{--Footer--}}
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

@endsection

@section('admin_script_codes')

<script type="text/javascript">
    $(document).ready(function() {
        $('#reply').summernote({height: 300});
    });
</script>
@endsection
    
