@extends('admin.layouts.master_admin')

@section('page_title')
{{config('app.name')}} | Edit Free Download
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit Free Download</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">  
                <form action="" method="post"
                        id="edit_business_form" enctype="multipart/form-data" data-parsley-validate>
                    @csrf
                    <fieldset>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Title <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="title"
                                    id="title" placeholder="Enter title" required="" data-parsley-required-message="Please enter title." value="{{$row->title}}">
                            @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label class="d-block" for="exampleInputEmail4">File</label>
                            @if($row->file!='')
                            <div class="">
                                <a href="{{url('storage/app').'/'.$row->file}}" target="_blank" class=""><i class="fa fa-file-pdf" style="font-size: 90px;"></i> </a>
                            </div>
                            @endif

                            <label class="d-block" for="exampleInputEmail4">Update File</label>
                            <input type="file" class="form-control img-upload-tag" name="file"
                                    id="file" data-parsley-required-message="Please upload file." data-parsley-pdfextension="pdf">
                            @if ($errors->has('file'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('file') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label class="d-block" for="exampleInputEmail4">Thumbnail</label>
                            @if($row->thumbnail!='')<img src="{{url('storage/app').'/'.$row->thumbnail}}" height="100" width="150">@endif

                            <label class="d-block" for="exampleInputEmail4">Update Thumbnail</label>
                            <input type="file" class="form-control img-upload-tag" name="thumbnail"
                                    id="thumbnail" data-parsley-required-message="Please upload thumbnail." data-parsley-fileextension="jpg,jpeg,png">
                            @if ($errors->has('thumbnail'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('thumbnail') }}</strong>
                                </span>
                            @endif
                        </div>


                        <input type="submit" class="btn btn-info" value="Save">

                    </fieldset>
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                {{--Footer--}}
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->
    </section>
    <!-- /.content -->
@endsection

@section('admin_script_codes')

@endsection
