@extends('admin.layouts.master_admin')

@section('page_title')
{{config('app.name')}} | Add Branch
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add Branch</h1>
                </div>

                
            </div>
        </div><!-- /.container-fluid -->
    </section>

 
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">   
                <form action="" method="post"
                        id="add_business_form" enctype="multipart/form-data" data-parsley-validate>
                    @csrf
                    <fieldset>
                        <div class="form-group">
                            <label for="exampleInputEmail4">Branch <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="branch"
                                    id="branch" placeholder="Enter branch" required=""  data-parsley-required-message="Please enter branch." value="{{old('branch')}}">
                            @if ($errors->has('branch'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('branch') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Status</label>
                            <select name="status" id="status" class="form-control" required=""  data-parsley-required-message="Please select status."  data-parsley-errors-container="#statusError">
                                <option value="1">Active</option>
                                <option value="0">De-Active</option>
                            </select>

                            <div id="statusError" class="mt-2"></div>

                            @if ($errors->has('status'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('status') }}</strong>
                                </span>
                            @endif
                        </div>

                        <input type="submit" class="btn btn-info" value="Save">

                    </fieldset>
                </form>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                {{--Footer--}}
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection

@section('admin_script_codes')
<script type="text/javascript">
    $(document).ready(function() {
        $('#description').summernote({height: 300});
    });
</script>


@endsection
