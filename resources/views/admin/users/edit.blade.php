@extends('admin.layouts.master_admin')

@section('page_title')
{{config('app.name')}} | Edit User
@endsection

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit User</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    
    <!--Begin Content-->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    
                </div>
            </div>
            <div class="card-body">
                <form action="" method="post"
                        id="add_business_term_form" enctype="multipart/form-data" data-parsley-validate>
                    @csrf
                    <fieldset>
                        <div class="form-group">
                            <label for="exampleInputEmail4">First Name</label>
                            <input type="text" class="form-control" name="first_name"
                                    id="first_name" placeholder="Enter first name"  required=""data-parsley-required-message="Please enter first name." value="{{$row->first_name}}">
                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                        <div class="form-group">
                            <label for="exampleInputEmail4">Last Name</label>
                            <input type="text" class="form-control" name="last_name"
                                    id="last_name" placeholder="Enter last name"  required=""data-parsley-required-message="Please enter last name." value="{{$row->last_name}}">
                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Email</label>
                            <input type="email" class="form-control" name="email"
                                    id="email" placeholder="Enter email" required="" data-parsley-required-message="Please enter email." data-parsley-type-message="Please enter a valid email." value="{{$row->email}}">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Phone</label>
                            <input type="text" class="form-control" name="phone"
                                    id="phone" placeholder="Enter phone" required="" data-parsley-required-message="Please enter phone." data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" data-parsley-pattern-message="Please enter a valid phone number." data-parsley-maxlength="15" data-parsley-maxlength-message="Please enter a valid phone number." value="{{$row->phone}}">
                            @if ($errors->has('phone'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Country<span class="text-danger"> *</span></label>
                            <select class="form-control{{ $errors->has('country_id') ? ' is-invalid' : '' }}" name="country_id" id="selectCountry" required="" data-parsley-required-message="Please select country">
                                <option value="">Select Country</option>
                                @foreach($countries as $country)
                                <option value="{{$country->id}}" @if($row->country_id==$country->id) selected @endif>{{$country->country}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('country'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('country') }}</strong>
                                </span>
                            @endif
                          </div>

                        <div class="form-group">
                            <label>State<span class="text-danger"> *</span></label>
                            <select class="form-control{{ $errors->has('state_id') ? ' is-invalid' : '' }}" name="state_id" id="selectState" required="" data-parsley-required-message="Please select state">
                                <option value="">Select State</option>
                                @foreach($states as $state)
                                <option value="{{$state->id}}" @if($row->state_id==$state->id) selected @endif>{{$state->state}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('state_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('state_id') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>City<span class="text-danger"> *</span></label>
                            <select class="form-control{{ $errors->has('city_id') ? ' is-invalid' : '' }}" name="city_id" id="selectCity" data-parsley-required-message="Please select city">
                                <option value="">Select City</option>
                                @foreach($cities as $city)
                                <option value="{{$city->id}}" @if($row->city_id==$city->id) selected @endif>{{$city->city}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('city_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('city_id') }}</strong>
                                </span>
                            @endif
                        </div>

                        <input type="submit" class="btn btn-danger" value="Save">

                    </fieldset>
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                {{--Footer--}}
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

@endsection

@section('admin_script_codes')

<script>
  

    $('#selectCountry').change(function(){
        $("#selectState").empty();
        $.get("{{URL::to('admin/fetch-states')}}", {country_id: $(this).val()}, function(res){
            // /console.log(res);
            $("#selectState").append(res);
        })
    });

    $('#selectState').change(function(){
        $("#selectCity").empty();
        $.get("{{URL::to('admin/fetch-cities')}}", {state_id: $(this).val()}, function(res){
            // /console.log(res);
            $("#selectCity").append(res);
        })
    });

</script>
@endsection
    
