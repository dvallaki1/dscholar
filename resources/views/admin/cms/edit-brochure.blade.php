@extends('admin.layouts.master_admin')

@section('page_title')
{{config('app.name')}} | Edit Brochure
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit Brochure</h1>
                </div>

                
            </div>
        </div><!-- /.container-fluid -->
    </section>

 
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">       
                            
                <form action="" method="post" id="edit_about_form" enctype="multipart/form-data" data-parsley-validate>
                    @csrf
                    <fieldset>

                        <div class="form-group">
                            <label class="d-block" for="exampleInputEmail4">Brochure</label>
                            @if($row->brochure!='')
                            <div class="">
                                <a href="{{url('storage/app').'/'.$row->brochure}}" download="" target="_blank" class=""><i class="fa fa-file-pdf" style="font-size: 90px;"></i> </a>
                            </div>
                            @endif

                            <label class="d-block" for="exampleInputEmail4">Update Brochure</label>
                            <input type="file" class="form-control img-upload-tag" name="brochure"
                                    id="brochure" data-parsley-pdfextension="pdf">
                            @if ($errors->has('brochure'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('brochure') }}</strong>
                                </span>
                            @endif
                        </div>

                        <input type="submit" class="btn btn-info" value="Save">

                    </fieldset>
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                {{--Footer--}}
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection

@section('admin_script_codes')

@endsection
    
