@extends('admin.layouts.master_admin')

@section('page_title')
{{config('app.name')}} | Edit Homepage
@endsection

@section('content')
<link rel="stylesheet" href="{{asset('public/assets/front-user/css/jquery.filer.css')}}">

<!-- Content Header (Page header) -->
<section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit Homepage</h1>
                </div>

                
            </div>
        </div><!-- /.container-fluid -->
    </section>

 
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <form action="" method="post" id="edit_homeppage_form" enctype="multipart/form-data" data-parsley-validate>
                    @csrf
                    <fieldset>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Title</label>
                            <input type="text" class="form-control" name="title"
                                    id="title" placeholder="Enter title" required=""  data-parsley-required-message="Please enter title." value="{{$row->title}}">
                            @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Subtitle</label>
                            <input type="text" class="form-control" name="sub_title"
                                    id="sub_title" placeholder="Enter sub title" required=""  data-parsley-required-message="Please enter sub title." value="{{$row->sub_title}}">
                            @if ($errors->has('sub_title'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('sub_title') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group ns-des show-maze-images">
                            <label for="exampleInputEmail4">Description</label> 
                            <textarea name="description" id="description" placeholder="Enter description" class="form-control" required="" data-parsley-required-message="Please enter description." data-parsley-errors-container="#descriptionError">{!! $row->description !!}</textarea>

                            <div id="descriptionError" class=""></div>
                                
                            @if ($errors->has('description'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                                </span>
                                @endif
                        </div>

                        <div class="form-group">
                            <label class="d-block" for="exampleInputEmail4">About Image</label>
                            @if($row->about_image!='')<img src="{{url('storage/app').'/'.$row->about_image}}" height="100" width="150">@endif

                            <label class="d-block" for="exampleInputEmail4">Update About Image</label>
                            <input type="file" class="form-control img-upload-tag" name="about_image"
                                    id="about_image" data-parsley-required-message="Please upload about image." data-parsley-fileextension="jpg,jpeg,png">
                            @if ($errors->has('about_image'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('about_image') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">About Title</label>
                            <input type="text" class="form-control" name="about_title"
                                    id="about_title" placeholder="Enter about title" required=""  data-parsley-required-message="Please enter about title." value="{{$row->about_title}}">
                            @if ($errors->has('about_title'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('about_title') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group ns-des show-maze-images">
                            <label for="exampleInputEmail4">About Description</label> 
                            <textarea name="about_description" id="about_description" placeholder="Enter about description" class="form-control" required="" data-parsley-required-message="Please enter about description." data-parsley-errors-container="#descriptionError">{!! $row->about_description !!}</textarea>

                            <div id="descriptionError" class=""></div>
                                
                            @if ($errors->has('about_description'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('about_description') }}</strong>
                                </span>
                                @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Slider Section Title</label>
                            <input type="text" class="form-control" name="slider_section_title"
                                    id="slider_section_title" placeholder="Enter slider section title" data-parsley-required-message="Please enter slider section title." value="{{$row->slider_section_title}}">
                            @if ($errors->has('slider_section_title'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('slider_section_title') }}</strong>
                                </span>
                            @endif
                        </div>

                        <input type="submit" class="btn btn-info" value="Save">

                    </fieldset>
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                {{--Footer--}}
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection
@section('admin_script_codes')
<script type="text/javascript">
    $(document).ready(function() {
        $('#description').summernote({height: 300});
        $('#about_description').summernote({height: 300});
    });
</script>
@endsection
    
