@extends('admin.layouts.master_admin')

@section('page_title')
{{config('app.name')}} | Edit About Page
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit About Page</h1>
                </div>

                
            </div>
        </div><!-- /.container-fluid -->
    </section>

 
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">       
                            
                <form action="" method="post" id="edit_about_form" enctype="multipart/form-data" data-parsley-validate>
                    @csrf
                    <fieldset>

                        <div class="form-group">
                            <label class="d-block" for="exampleInputEmail4">Image</label>
                            @if($row->image!='')<img src="{{url('storage/app').'/'.$row->image}}" height="100" width="150">@endif

                            <label class="d-block" for="exampleInputEmail4">Update Image</label>
                            <input type="file" class="form-control img-upload-tag" name="image"
                                    id="image" data-parsley-required-message="Please upload image." data-parsley-fileextension="jpg,jpeg,png">
                            @if ($errors->has('image'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('image') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group ns-des show-maze-images">
                            <label for="exampleInputEmail4">Description</label> 
                            <textarea name="description" id="description" placeholder="Enter description" class="form-control" required="" data-parsley-required-message="Please enter description." data-parsley-errors-container="#descriptionError">{!! $row->description !!}</textarea>

                            <div id="descriptionError" class=""></div>
                                
                            @if ($errors->has('description'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                                </span>
                                @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Left Title</label>
                            <input type="text" class="form-control" name="left_title"
                                    id="left_title" placeholder="Enter left title" required=""  data-parsley-required-message="Please enter left title." value="{{$row->left_title}}">
                            @if ($errors->has('left_title'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('left_title') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group ns-des show-maze-images">
                            <label for="exampleInputEmail4">Left Description</label> 
                            <textarea name="left_description" id="left_description" placeholder="Enter left description" class="form-control" required="" data-parsley-required-message="Please enter left description." data-parsley-errors-container="#descriptionError">{!! $row->left_description !!}</textarea>

                            <div id="descriptionError" class=""></div>
                                
                            @if ($errors->has('left_description'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('left_description') }}</strong>
                                </span>
                                @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Right Title</label>
                            <input type="text" class="form-control" name="right_title"
                                    id="right_title" placeholder="Enter right title" required=""  data-parsley-required-message="Please enter right title." value="{{$row->right_title}}">
                            @if ($errors->has('right_title'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('right_title') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group ns-des show-maze-images">
                            <label for="exampleInputEmail4">Right Description</label> 
                            <textarea name="right_description" id="right_description" placeholder="Enter right description" class="form-control" required="" data-parsley-required-message="Please enter right description." data-parsley-errors-container="#descriptionError">{!! $row->right_description !!}</textarea>

                            <div id="descriptionError" class=""></div>
                                
                            @if ($errors->has('right_description'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('right_description') }}</strong>
                                </span>
                                @endif
                        </div>

                        <input type="submit" class="btn btn-info" value="Save">

                    </fieldset>
                </form>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                {{--Footer--}}
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection
@section('admin_script_codes')
<script type="text/javascript">
    $(document).ready(function() {
        $('#description').summernote({height: 300});
        $('#left_description').summernote({height: 300});
        $('#right_description').summernote({height: 300});
    });
</script>
@endsection
    
