@extends('admin.layouts.master_admin')

@section('page_title')
{{config('app.name')}} | Add Homepage Video
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add Homepage Video</h1>
                </div>

                
            </div>
        </div><!-- /.container-fluid -->
    </section>

 
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">   
                <form action="" method="post"
                        id="add_business_form" enctype="multipart/form-data" data-parsley-validate>
                    @csrf
                    <fieldset>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Video </label>
                            <video controls="" src="{{url('storage/app').'/'.$row->video}}" height="200px" width="200px"></video>
                        </div>
                        
                        <div class="form-group">
                            <label for="exampleInputEmail4">Update Video </label>
                            <input type="file" class="form-control img-upload-tag" name="video" id="video" data-parsley-videoextension="mp4,flv,avi,mpeg">
                            @if ($errors->has('video'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('video') }}</strong>
                                </span>
                            @endif
                        </div>

                        <input type="submit" class="btn btn-info" value="Save">

                    </fieldset>
                </form>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                {{--Footer--}}
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection

@section('admin_script_codes')

@endsection
