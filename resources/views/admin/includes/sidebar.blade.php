
<div  class="top-secret">
    <aside id="main-sidebar" class="main-sidebar sidebar-dark-primary elevation-4 sidebarSmal">
        <!-- Brand Logo -->
        <a href="{{url('/')}}" class="brand-link">
            <img src="{{asset('public/assets/admin/img/AdminLTELogo.png')}}" class="brand-image img-circle elevation-3"
                style="opacity: .8">
            <span class="brand-text font-weight-light">{{config('app.name')}} | Admin</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">

                    <?php
                    if(Auth::check() && Auth::guard('admin')->check() && (Auth::User()->profile_pic == '' || Auth::User()->profile_pic == null))
                        $src = URL::asset('public')."/assets/admin/img/default-profile-pic.png";
                    else
                        $src = URL::to('storage/app')."/".Auth::User()->profile_pic;
                    ?>

                    <img src="<?php echo ($src); ?>" class="img-circle elevation-2" id="adminProfileImgSidebar">
                </div>
                <div class="info">
                    <a href="#" class="d-block">{{ ucfirst(Auth::User()->first_name)}} {{ucfirst(Auth::User()->last_name)}}</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                        with font-awesome or any other icon font library -->

                    <li class="nav-item has-treeview">
                        <a href="{{URL::to('admin')}}" class="nav-link {{ (Request::segment(1) === 'admin' && Request::segment(2) === '' ) ? 'active' : '' }}">
                            <i class="fas fa-tachometer-alt"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="fas fa-users"></i>

                            <p>
                                Manage Users
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{URL::to('admin/manage-users')}}" class="nav-link {{ URL::to('admin/manage-users') == url()->current() || URL::to('admin/add-user') == url()->current() || URL::to('admin/edit-user').'/'.collect(request()->segments())->last() == url()->current() ? 'active' : ' ' }}">
                                <i class="fas fa-user-alt"></i>

                                    <p>Manage Users</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="fas fa-clipboard-list"></i>

                            <p>
                                Manage Videos
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{URL::to('admin/manage-branches')}}" class="nav-link {{ URL::to('admin/manage-branches') == url()->current() || URL::to('admin/add-branch') == url()->current() || URL::to('admin/edit-branch').'/'.collect(request()->segments())->last() == url()->current() ? 'active' : ' ' }}">
                                    <i class="fas fa-tasks"></i>

                                    <p>Manage Branches</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{URL::to('admin/manage-subjects')}}" class="nav-link {{ URL::to('admin/manage-subjects') == url()->current() || URL::to('admin/add-subject') == url()->current() || URL::to('admin/edit-subject').'/'.collect(request()->segments())->last() == url()->current() ? 'active' : ' ' }}">
                                    <i class="fas fa-tasks"></i>

                                    <p>Manage Subjects</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{URL::to('admin/manage-videos')}}" class="nav-link {{ URL::to('admin/manage-videos') == url()->current() || URL::to('admin/add-video') == url()->current() || URL::to('admin/edit-video').'/'.collect(request()->segments())->last() == url()->current() ? 'active' : ' ' }}">
                                    <i class="fas fa-tasks"></i>

                                    <p>Manage Videos</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{URL::to('admin/manage-free-downloads')}}" class="nav-link {{ URL::to('admin/manage-free-downloads') == url()->current() || URL::to('admin/add-free-download') == url()->current() || URL::to('admin/edit-free-download').'/'.collect(request()->segments())->last() == url()->current() ? 'active' : ' ' }}">
                                    <i class="fas fa-tasks"></i>

                                    <p>Free Downloads</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{URL::to('admin/manage-discount-codes')}}" class="nav-link {{ URL::to('admin/manage-discount-codes') == url()->current() || URL::to('admin/add-discount-code') == url()->current() || URL::to('admin/edit-discount-code').'/'.collect(request()->segments())->last() == url()->current() ? 'active' : ' ' }}">
                                    <i class="fas fa-tasks"></i>

                                    <p>Discount Codes</p>
                                </a>
                            </li>
                        </ul>
                    </li>


                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="fas fa-clipboard-list"></i>

                            <p>
                                Manage CMS
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            
                            <li class="nav-item">
                                <a href="{{URL::to('admin/manage-blogs')}}" class="nav-link {{ URL::to('admin/manage-blogs') == url()->current() || URL::to('admin/add-blog') == url()->current() || URL::to('admin/edit-blog').'/'.collect(request()->segments())->last() == url()->current() ? 'active' : ' ' }}">
                                    <i class="fas fa-tasks"></i>

                                    <p>Manage Blogs</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{URL::to('admin/manage-faqs')}}" class="nav-link {{ URL::to('admin/manage-faqs') == url()->current() || URL::to('admin/add-faq') == url()->current() || URL::to('admin/edit-faq').'/'.collect(request()->segments())->last() == url()->current() ? 'active' : ' ' }}">
                                    <i class="fas fa-tasks"></i>

                                    <p>Manage FAQs</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{URL::to('admin/edit-brochure')}}" class="nav-link {{ URL::to('admin/edit-brochure') == url()->current() ? 'active' : ' ' }}">
                                    <i class="fas fa-tasks"></i>

                                    <p>Brochure</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{URL::to('admin/edit-homepage')}}" class="nav-link {{ URL::to('admin/edit-homepage') == url()->current() ? 'active' : ' ' }}">
                                    <i class="fas fa-tasks"></i>

                                    <p>Home Page</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{URL::to('admin/manage-homepage-videos')}}" class="nav-link {{ URL::to('admin/manage-homepage-videos') == url()->current() || URL::to('admin/add-homepage-video') == url()->current() || URL::to('admin/edit-homepage-video').'/'.collect(request()->segments())->last() == url()->current() ? 'active' : ' ' }}">
                                    <i class="fas fa-tasks"></i>

                                    <p>Homepage Videos</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{URL::to('admin/manage-homepage-upcomings')}}" class="nav-link {{ URL::to('admin/manage-homepage-upcomings') == url()->current() || URL::to('admin/add-homepage-upcoming') == url()->current() || URL::to('admin/edit-homepage-upcoming').'/'.collect(request()->segments())->last() == url()->current() ? 'active' : ' ' }}">
                                    <i class="fas fa-tasks"></i>

                                    <p>Homepage Sliders</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{URL::to('admin/edit-about')}}" class="nav-link {{ URL::to('admin/edit-about') == url()->current() ? 'active' : ' ' }}">
                                    <i class="fas fa-tasks"></i>

                                    <p>About Page</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{URL::to('admin/manage-pages')}}" class="nav-link {{ URL::to('admin/manage-pages') == url()->current() || URL::to('admin/add-page') == url()->current() || URL::to('admin/edit-page').'/'.collect(request()->segments())->last() == url()->current() ? 'active' : ' ' }}">
                                    <i class="fas fa-tasks"></i>

                                    <p>Manage Other Pages</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{URL::to('admin/manage-email-contents')}}" class="nav-link {{ URL::to('admin/manage-email-contents') == url()->current() || URL::to('admin/edit-email-content').'/'.collect(request()->segments())->last() == url()->current() ? 'active' : ' ' }}">
                                    <i class="fas fa-tasks"></i>

                                    <p>Manage Email Content</p>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="fas fa-clipboard-list"></i>

                            <p>
                                Support
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{URL::to('admin/manage-enquiries')}}" class="nav-link {{ URL::to('admin/manage-enquiries') == url()->current() || URL::to('admin/view-enquiry').'/'.collect(request()->segments())->last() == url()->current() || URL::to('admin/reply-enquiry').'/'.collect(request()->segments())->last() == url()->current() ? 'active' : ' ' }}">
                                <i class="fas fa-user-alt"></i>

                                    <p>Contact Enquiries</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{URL::to('admin/manage-emails')}}" class="nav-link {{ URL::to('admin/manage-emails') == url()->current() || URL::to('admin/add-email') == url()->current() || URL::to('admin/edit-email').'/'.collect(request()->segments())->last() == url()->current() ? 'active' : ' ' }}">
                                    <i class="fas fa-tasks"></i>

                                    <p>Send Emails</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
</div>


<script>
$(document).ready(function(){
    if ($('.nav-treeview li a.nav-link').hasClass('active')) {
        $('.nav-treeview li a.nav-link.active').parent().parent().parent().addClass('menu-open');
    }
});


</script>