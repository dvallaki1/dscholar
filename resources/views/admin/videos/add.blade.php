@extends('admin.layouts.master_admin')

@section('page_title')
{{config('app.name')}} | Add Video
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add Video</h1>
                </div>

                
            </div>
        </div><!-- /.container-fluid -->
    </section>

 
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">   
                <form action="" method="post"
                        id="add_business_form" enctype="multipart/form-data" data-parsley-validate>
                    @csrf
                    <fieldset>

                        <div class="form-group">
                            <label>Subject<span class="text-danger"> *</span></label>
                            <select class="form-control{{ $errors->has('subject_id') ? ' is-invalid' : '' }}" name="subject_id" id="selectSubject" required="" data-parsley-required-message="Please select subject">
                                <option value="">Select Subject</option>
                                @foreach($subjects as $subject)
                                <option value="{{$subject->id}}" @if(old('subject_id')==$subject->id) selected @endif>{{$subject->subject}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('subject'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('subject') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Title <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="title"
                                    id="title" placeholder="Enter title" required=""  data-parsley-required-message="Please enter title." value="{{old('title')}}">
                            @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Subtitle <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="subtitle"
                                    id="subtitle" placeholder="Enter subtitle" required=""  data-parsley-required-message="Please enter subtitle." value="{{old('subtitle')}}">
                            @if ($errors->has('subtitle'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('subtitle') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Duration </label>
                            <input type="text" class="form-control" name="duration"
                                    id="duration" placeholder="Enter duration" data-parsley-required-message="Please enter duration." value="{{old('duration')}}">
                            @if ($errors->has('duration'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('duration') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Path <span class="text-danger">*</span></label>
                            <input type="file" class="form-control img-upload-tag" name="path" id="path" required=""  data-parsley-required-message="Please upload video" data-parsley-videoextension="mp4,flv,avi,mpeg">
                            @if ($errors->has('path'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('path') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Thumbnail <span class="text-danger">*</span></label>
                            <input type="file" class="form-control img-upload-tag" name="thumbnail" id="thumbnail" required=""  data-parsley-required-message="Please upload thumbnail" data-parsley-fileextension="jpg,jpeg,png">
                            @if ($errors->has('thumbnail'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('thumbnail') }}</strong>
                                </span>
                            @endif
                        </div>

                        <input type="submit" class="btn btn-info" value="Save">

                    </fieldset>
                </form>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                {{--Footer--}}
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection

@section('admin_script_codes')
<script type="text/javascript">
    $(document).ready(function() {
        $('#description').summernote({height: 300});
    });
</script>


@endsection
