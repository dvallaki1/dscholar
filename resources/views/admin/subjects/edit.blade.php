@extends('admin.layouts.master_admin')

@section('page_title')
{{config('app.name')}} | Edit Subject
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit Subject</h1>
                </div>

                
            </div>
        </div><!-- /.container-fluid -->
    </section>

 
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">   
                <form action="" method="post"
                        id="add_business_form" enctype="multipart/form-data" data-parsley-validate>
                    @csrf
                    <fieldset>
                        <div class="form-group">
                            <label>Branch<span class="text-danger"> *</span></label>
                            <select class="form-control{{ $errors->has('branch_id') ? ' is-invalid' : '' }}" name="branch_id" id="selectBranch" required="" data-parsley-required-message="Please select branch">
                                <option value="">Select Branch</option>
                                @foreach($branches as $branch)
                                <option value="{{$branch->id}}" @if($row->branch_id==$branch->id) selected @endif>{{$branch->branch}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('branch'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('branch') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                        <div class="form-group">
                            <label for="exampleInputEmail4">Subject <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="subject"
                                    id="subject" placeholder="Enter subject" required=""  data-parsley-required-message="Please enter subject." value="{{$row->subject}}">
                            @if ($errors->has('subject'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('subject') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Price <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="price"
                                    id="price" placeholder="Enter price" required="" data-parsley-required-message="Please enter price." value="{{$row->price}}">
                            @if ($errors->has('price'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('price') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Subscription Duration (months) <span class="text-danger">*</span></label>
                            <select class="form-control" name="duration_in_months"
                                    id="duration_in_months" required="" data-parsley-required-message="Please select subscription duration.">
                                        <option value="">Select</option>
                                        <option value="1" @if($row->duration_in_months==1) selected="" @endif>1</option>
                                        <option value="2" @if($row->duration_in_months==2) selected="" @endif>2</option>
                                        <option value="3" @if($row->duration_in_months==3) selected="" @endif>3</option>
                                        <option value="4" @if($row->duration_in_months==4) selected="" @endif>4</option>
                                        <option value="5" @if($row->duration_in_months==5) selected="" @endif>5</option>
                                        <option value="6" @if($row->duration_in_months==6) selected="" @endif>6</option>
                                    </select>
                            @if ($errors->has('duration_in_months'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('duration_in_months') }}</strong>
                                </span>
                            @endif
                        </div>

                        <input type="submit" class="btn btn-info" value="Save">

                    </fieldset>
                </form>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                {{--Footer--}}
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection

@section('admin_script_codes')

@endsection
