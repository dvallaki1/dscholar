@extends('admin.layouts.master_admin')

@section('page_title')
{{config('app.name')}} | Send Email to Users
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Send Email to Users</h1>
                </div>

                
            </div>
        </div><!-- /.container-fluid -->
    </section>

 
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">   
                <form action="" method="post"
                        id="add_business_form" enctype="multipart/form-data" data-parsley-validate>
                    @csrf
                    <fieldset>
                        <div class="form-group">
                            <label for="user_id">Users</label>
                            <select class="form-control" name="user_ids[]" id="user_id" multiple="">
                                <option value="">Select Users</option>
                                @foreach($users as $user)
                                <option value="{{$user->id}}" selected="">{{$user->first_name.' '.$user->last_name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('user_id'))
                                <span class="invalid-feedback" role="email">
                                <strong>{{ $errors->first('user_id') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Subject</label>
                            <input type="text" class="form-control" name="subject"
                                   id="subject" placeholder="Enter subject"  data-parsley-error-message="Please enter subject." required="" >
                            @if ($errors->has('subject'))
                                <span class="invalid-feedback" role="email">
                                <strong>{{ $errors->first('subject') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Body</label> <textarea
                                    name="body" id="body"
                                    placeholder="Enter body"
                                    class="form-control" required=""  data-parsley-error-message="Please enter body."></textarea>
                            @if ($errors->has('body'))
                                <span class="invalid-feedback" role="email">
                                <strong>{{ $errors->first('body') }}</strong>
                                </span>
                            @endif
                        </div>

                        <input type="submit" class="btn btn-info" value="Save">

                    </fieldset>
                </form>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                {{--Footer--}}
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection

@section('admin_script_codes')
<script type="text/javascript">
    $(document).ready(function() {
        $('#body').summernote({height: 300});
    });
</script>


@endsection
