@extends('admin.layouts.master_admin')

@section('page_title')
{{config('app.name')}} | Manage Discount Codes
@endsection


@section('content')



    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Manage Discount Codes</h1>
                </div>

                <div class="col-sm-6 text-right">
                    <a href="{{Request::root()}}/admin/add-discount-code" class="btn btn-success btn-gold-styled pull-right"><i class="fa fa-plus"></i> Add Discount Code</a>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" id="example1">
                        <thead>
                        <tr>
                            <th class="column-title">Created At </th>
                            <th class="column-title">Code </th>
                            <th class="column-title">Discount </th>
                            <th class="column-title">Date Range </th>
                            <th class="column-title text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1;  ?>
                        @foreach($result as $row)
                            <tr>

                                <td>{{ $row->created_at }}</td>
                                <td>{{ $row->code }}</td>
                                <td>
                                    @php
                                    if($row->type == 'Fixed')
                                    $discount = '<i class="fa fa-rupee-sign"></i>'.$row->value;
                                    else
                                    $discount = $row->value.'%';
                                    @endphp
                                    {!! $discount !!}
                                </td>
                                <td>
                                    @if($row->from_date==null && $row->to_date==null) 
                                        Indefinite 
                                    @else
                                        {{date('d/m/Y',strtotime($row->from_date)).' - '.date('d/m/Y',strtotime($row->to_date))}}
                                    @endif
                                </td>
                                <td class="text-center" style="white-space:nowrap;">
                                    <a href="{{URL::to('/admin/edit-discount-code',['id'=>$row->id])}}" title="Edit"><i class="fa fa-edit fa-fw fa-lg"></i></a>
                                    
                                    <a href="javascript:void(0);" class="deleteDiscountCode" id="{{$row->id}}" title="Delete"><i class="fa fa-trash fa-fw fa-lg"></i></a>

                                </td>

                            </tr>
                            <?php $i++; ?>
                        @endforeach
                        
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                {{--Footer--}}
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection


@section('admin_script_links')  
@endsection
@section('admin_script_codes')
<script>
    $(document).ready(function() {
        $('#example1').DataTable(
        {
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [4],
                    "orderable": false
                }
            ]
        });
    } );
</script>
<script type="text/javascript">
    $(document).ready(function () {

            $("#example1").on("click", ".deleteDiscountCode", function (e) {

                e.preventDefault();
                let id = $(this).attr('id');
                swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this FAQ Code!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel please!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                type: "post",
                                url: "{{ url('/admin/delete-discount-code') }}",
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                    "id": id
                                },
                                success: function (response) {

                                    if (response.status == "success") {
                                        toastr.success(response.msg);

                                        setTimeout(function () {
                                            location.reload();
                                        }, 5000)

                                    }
                                    if (response.status == "error") {
                                        toastr.info(response.msg);
                                        setTimeout(function () {
                                            location.reload();
                                        }, 5000)
                                    }
                                }
                            });
                            swal("Deleted!", "FAQ Code deleted successfully.", "success");
                        } else {
                            swal("Cancelled", "FAQ Code is safe :)", "error");
                        }
                    });
            });
        });

</script>
@endsection