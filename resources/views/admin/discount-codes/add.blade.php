@extends('admin.layouts.master_admin')

@section('page_title')
{{config('app.name')}} | Add Discount Code
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Add Discount Code</h1>
                </div>

                
            </div>
        </div><!-- /.container-fluid -->
    </section>

 
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            <div class="card-body">   
                <form action="" method="post"
                        id="add_business_form" enctype="multipart/form-data" data-parsley-validate>
                    @csrf
                    <fieldset>
                        <div class="form-group">
                            <label for="exampleInputEmail4">Code <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="code"
                                    id="code" placeholder="Enter code" required="" data-parsley-required-message="Please enter code." value="{{old('code')}}">
                            @if ($errors->has('code'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('code') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Type</label>
                            <select name="type" id="type" class="form-control" required=""  data-parsley-required-message="Please select type."  data-parsley-errors-container="#typeError">
                                <option value="Percent">Percent</option>
                                <option value="Fixed">Fixed</option>
                            </select>

                            <div id="typeError" class="mt-2"></div>

                            @if ($errors->has('type'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('type') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">Value <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="value"
                                    id="value" placeholder="Enter value" required="" data-parsley-required-message="Please enter value." value="{{old('value')}}">
                            @if ($errors->has('value'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('value') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">From Date <span class="text-danger">*</span></label>
                            <input type="date" class="form-control" name="from_date"
                                    id="from_date" placeholder="Enter from date" required="" data-parsley-required-message="Please enter from date." value="{{old('from_date')}}">
                            @if ($errors->has('from_date'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('from_date') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail4">To Date <span class="text-danger">*</span></label>
                            <input type="date" class="form-control" name="to_date"
                                    id="to_date" placeholder="Enter to date" required="" data-parsley-required-message="Please enter to date." value="{{old('to_date')}}">
                            @if ($errors->has('to_date'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('to_date') }}</strong>
                                </span>
                            @endif
                        </div>

                        <input type="submit" class="btn btn-info" value="Save">

                    </fieldset>
                </form>

            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                {{--Footer--}}
            </div>
            <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection

@section('admin_script_codes')

@endsection
