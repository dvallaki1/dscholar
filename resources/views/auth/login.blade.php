
@extends('layouts.master_user')

@section('content')
<main>
    <!-- Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Login</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero Area End-->
    <!--================login_part Area =================-->
    <section class="login_part ">
        <div class="container">
            <div class="row align-items-center">                    
                <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                    <div class="login_part_form">
                        <div class="login_part_form_iner">
                           
                            <form class="row contact_form"  method="post" novalidate="novalidate" data-parsley-validate>
                                @csrf
                                <div class="col-md-12 form-group p_star">
                                    <input type="email" class="form-control" value=""
                                        placeholder="Email" name="email" required="" data-parsley-required-message="Please enter email." data-parsley-type-message="Please enter a valid email.">
                                </div>
                                <div class="col-md-12 form-group p_star">
                                    <input type="password" class="form-control form-group" placeholder="Password" name="password" required="" data-parsley-required-message="Please enter password.">
                                </div>
                                <div class="col-md-12 form-group">                                    
                                    <button type="submit" value="submit" class="border-btn">
                                        Log in
                                    </button>
                                    <a class="lost_pass" href="{{url('forgot-password')}}">Forgot password?</a>
                                </div>
                                </form>

                                <div class="col-md-12 form-group">   
                                    New Member? <a href="{{url('register')}}" class="link">Sign Up</a> 
                                </div> 

                                <div style="text-align: center;">OR</div>

                                <div class="fcol-md-12 form-group" style="text-align: center;">
                                    <a class="border-btn" href="{{url('login/google')}}">Login with Google</a>
                                    <a class="border-btn" href="{{url('login/facebook')}}">Login with Facebook</a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================login_part end =================-->
</main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
@endsection