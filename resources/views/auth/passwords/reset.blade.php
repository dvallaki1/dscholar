
@extends('layouts.master_user')

@section('content')
<main>
    <!-- Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Reset Password</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero Area End-->
    <!--================login_part Area =================-->
    <section class="login_part ">
        <div class="container">
            <div class="row align-items-center">                    
                <div class="col-lg-6 offset-md-3 col-md-6">
                    <div class="login_part_form">
                        <div class="login_part_form_iner">
                            <form class="row contact_form"  method="post" novalidate="novalidate" data-parsley-validate>
                                @csrf
                                <div class="col-md-12 form-group p_star">
                                    <input type="password" class="form-control" id="password" placeholder="Password" name="password" required data-parsley-required-message="Please enter password." value="@if (!$errors->has('password')){{old('password')}}@endif">
                                </div>
                                <div class="col-md-12 form-group p_star">
                                    <input type="password" class="password1 form-control" id="reEnterPassword" name="password_confirmation" data-parsley-equalto="#password" data-parsley-equalto-message="Password and confirm password should be same." required data-parsley-required-message="Please re-enter password." placeholder="Re-enter password" value="@if (!$errors->has('password_confirmation')){{old('password_confirmation')}}@endif">
                                </div>                            
                                <div class="col-md-12 form-group">                                        
                                    <button type="submit" value="submit" class="border-btn">
                                        Submit
                                    </button>                                        
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================login_part end =================-->
</main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
@endsection
