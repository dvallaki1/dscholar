
@extends('layouts.master_user')

@section('content')
<main>
    <!-- Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Forgot Password</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero Area End-->
    <!--================login_part Area =================-->
    <section class="login_part ">
        <div class="container">
            <div class="row align-items-center">                    
                <div class="col-lg-6 offset-md-3 col-md-6">
                    <div class="login_part_form">
                        <div class="login_part_form_iner">
                            <p>Please fill out your email. A link to reset password will be sent there.</p>
                            <form class="row contact_form"  method="post" novalidate="novalidate" data-parsley-validate>
                                @csrf
                                <div class="col-md-12 form-group p_star">
                                    <input type="email" class="form-control" value=""
                                        placeholder="Email" name="email" required="" data-parsley-required-message="Please enter email." data-parsley-type-message="Please enter a valid email.">
                                </div>                                  
                                <div class="col-md-12 form-group">                                        
                                    <button type="submit" value="submit" class="border-btn">
                                        Submit
                                    </button>                                        
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================login_part end =================-->
</main>
@endsection

@section('script_links')


@endsection

@section('script_codes')
@endsection
