
@extends('layouts.master_user')

@section('content')
<main>
    <!-- Hero Area Start-->
    <div class="slider-area ">
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="hero-cap text-center">
                            <h2>Create an Account</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hero Area End-->
    <!--================login_part Area =================-->
    <section class="login_part">
        <div class="container">
            <div class="row align-items-center">
                
               <div class="col-lg-6 offset-md-3 col-md-6">
                    <div class="login_part_form">
                        <div class="login_part_form_iner">
                            
                            <form class="row contact_form"  method="post" novalidate="novalidate" data-parsley-validate>
                                @csrf
                                <div class="col-md-12 form-group p_star">
                                    <input type="text" class="form-control" name="first_name"
                                    id="first_name" placeholder="Enter first name" required=""  data-parsley-required-message="Please enter first name." value="{{old('first_name')}}">
                                </div>
                                <div class="col-md-12 form-group p_star">
                                    <input type="text" class="form-control" name="last_name"
                                    id="last_name" placeholder="Enter last name" required=""  data-parsley-required-message="Please enter last name." value="{{old('last_name')}}">
                                </div>
                                <div class="col-md-12 form-group p_star">
                                    <input type="email" class="form-control" name="email"
                                    id="email" placeholder="Enter email" required=""  data-parsley-required-message="Please enter email."data-parsley-type-message="Please enter a valid email." value="{{old('email')}}"> 
                                </div>
                                <div class="col-md-12 form-group p_star">
                                    <input type="text" class="form-control" name="phone"
                                    id="phone" placeholder="Enter phone" required="" data-parsley-required-message="Please enter phone." data-parsley-pattern="^[\d\+\-\.\(\)\/\s]*$" data-parsley-pattern-message="Please enter a valid phone number." data-parsley-maxlength="15" data-parsley-maxlength-message="Please enter a valid phone number." value="{{old('phone')}}">
                                </div>
                                <div class="col-md-12 form-group p_star  plainRow">
                                    <input type="password" class="form-control" id="password" placeholder="Password" name="password" required data-parsley-required-message="Please enter password." value="@if (!$errors->has('password')){{old('password')}}@endif">
                                </div>
                                <div class="col-md-12 form-group p_star plainRow">
                                    <input type="password" class="password1 form-control" id="reEnterPassword" name="password_confirmation" data-parsley-equalto="#password" data-parsley-equalto-message="Password and confirm password should be same." required data-parsley-required-message="Please re-enter password." placeholder="Re-enter password" value="@if (!$errors->has('password_confirmation')){{old('password_confirmation')}}@endif">
                                </div>
                                
                                <div class="col-md-12 form-group p_star plainRow">                                   
                                    <div class="form-select form-control form-group p-0" id="default-select">
                                        <select class="form-control{{ $errors->has('country_id') ? ' is-invalid' : '' }}" name="country_id" id="selectCountry" required="" data-parsley-required-message="Please select country">
                                            <option value="">Select Country</option>
                                            @foreach($countries as $country)
                                            <option value="{{$country->id}}" @if(old('country_id')==$country->id) selected @endif>{{$country->country}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 form-group p_star plainRow">                                   
                                    <div class="form-select form-control form-group p-0" id="default-select">
                                        <select class="form-control{{ $errors->has('state_id') ? ' is-invalid' : '' }}" name="state_id" id="selectState" required="" data-parsley-required-message="Please select state">
                                            <option value="">Select State</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-12 form-group p_star">                                   
                                    <div class="form-select form-control form-group p-0" id="default-select">
                                        <select class="form-control{{ $errors->has('city_id') ? ' is-invalid' : '' }}" name="city_id" id="selectCity" data-parsley-required-message="Please select city">
                                            <option value="">Select City</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-12 form-group">
                                    
                                  <button type="submit" value="submit" class="border-btn">
                                       Register
                                    </button>
                                   
                                </div>
                                
                                 <div class="col-md-12 form-group">   
                                    Already Member? <a href="{{url('login')}}" class="link">Login</a> 
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================login_part end =================-->
</main>
@endsection

@section('script_links')


@endsection

@section('script_codes')

<script>
  

    $('#selectCountry').change(function(){
        $("#selectState").empty();
        $.get("{{URL::to('fetch-states')}}", {country_id: $(this).val()}, function(res){
            // /console.log(res);
            $("#selectState").append(res);
        })
    });

    $('#selectState').change(function(){
        $("#selectCity").empty();
        $.get("{{URL::to('fetch-cities')}}", {state_id: $(this).val()}, function(res){
            // /console.log(res);
            $("#selectCity").append(res);
        })
    });

</script>
@endsection
  