<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
            @if(isset($data['subscribe']))
        	<table style="width:100%;padding:10px;font-family:Calibri,Verdana,Ariel,sans-serif;color:#000000;font-size:14px;line-height:2em;">
                <tr>
					<td>Dear {{$data['name']}},
                        <p> You have successfully subscribed to our newsletter. <br/>
						</p>
                        <a href="{{Request::root().'/unsubscribe-newsletter/'.base64_encode($data['email'])}}">Click here </a>to unsubscribe
					</td>
				</tr>
                <tr>
                    <td>Cheers, <br/>
                    <strong>{{config('app.name')}} Team.</strong> <br/>
                    </td>
                </tr>
			</table>
            @elseif(isset($data['unsubscribe']))
            <table style="width:100%;padding:10px;font-family:Calibri,Verdana,Ariel,sans-serif;color:#000000;font-size:14px;line-height:2em;">
                <tr>
                    <td>Dear user,
                        <p> You have successfully unsubscribed from our newsletter. <br/>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>Cheers, <br/>
                    <strong>{{config('app.name')}} Team.</strong> <br/>
                    </td>
                </tr>
            </table>
            @endif
		</div>
	</body>
</html>

