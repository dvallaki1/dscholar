<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div class="mail-theme" style="width:700px; border: 10px solid #1e2162;">
			<div style="padding:36px 0 30px 0; background:#152463; text-align:center;">
			<img src="{{asset('public/assets/img/logo.png')}}" style="height:40px; width:100px;" alt="">	
			</div>	
			<div>
				<table style="padding:30px; width:100%;font-family:Calibri,Verdana,Ariel,sans-serif;color:#000000;font-size:14px;line-height:2em;">
					<tr>
						<td><h1 style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; color: #3d4852; font-size: 19px; font-weight: bold; margin-top: 0; text-align: left;">Dear User,</h1>
                       
                        <p style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; color: #3d4852; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">{{$data['body']}}</p>
                        <p style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; color: #3d4852; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">Thank you for using our application!</p>
					</td>
				</tr>
                <tr>
                    <td style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; color: #3d4852; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">Regards, <br/>
                    <strong style="font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; box-sizing: border-box; color: #3d4852; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">{{config('app.name')}}</strong> <br/>
                    </td>
                </tr>
			</table>
		</div>
		<table class="footer" style="padding:30px; background:#152463; color:#fff;" align="center" width="100%" cellpadding="0" cellspacing="0" role="presentation">
			<tr>
				<td class="content-cell" align="center">
					© {{date('Y')}} {{config('app.name')}}. All rights reserved.
				</td>
			</tr>
		</table>	

	</div>
	</body>
</html>

