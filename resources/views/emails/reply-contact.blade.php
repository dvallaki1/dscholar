<table class="wrapper" width="570" align="center" cellpadding="0" cellspacing="0" role="presentation" style="width: 570px;border: 1px solid #0A228F;">
    <tbody>
    <tr>
        <td class="header" style="text-align: center; padding:10px 0; border-bottom: 1px solid #0A228F">
        <a href="{{ url('/') }}">
        <img src="{{asset('public/assets/img/logo.png')}}" alt="" style="width: 180px;">
        </a>
        </td>
    </tr>
    <tr>
        <td style="border-top:solid 1px #e9d1e7; padding:43px 37px;">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="text-align: left;color:#2c2c2c; font-size: 25px;font-weight: bold;">
                        Hello, {{ucfirst($contact->name)}}, 
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;color:#616161; font-size: 14px;padding:27px 0 30px;line-height: 26px;">
                        This is with reference to your contact enquiry:
                        {{$contact->message}}
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;padding-bottom: 28px;">
                        <p>{!! $reply !!}</p>
                    </td>
                </tr>


                <tr>
                    <td style="color:#616161; font-size: 14px;padding: 39px 0 11px;">Regards,</td>
                </tr>
                <tr>
                    <td style="color:#616161; font-size: 14px;">{{config('app.name')}}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
<td>
<table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td class="content-cell" align="center" style="border-top: 1px solid #0A228F; padding: 10px 0;">
    <ul style="margin: 0; padding: 0; text-align: center;">
        <li style="list-style: none; display: inline-block;margin: 0 5px;"><a href=""><img src="https://dotbleu.alkurn.co.in/public/assets/front-end/images/facebook.png" alt="" style="width:35px;"></a></li>
        <li style="list-style: none; display: inline-block;margin: 0 5px;"><a href=""><img src="https://dotbleu.alkurn.co.in/public/assets/front-end/images/twitter.png" alt="" style="width:35px;"></a></li>
        <li style="list-style: none; display: inline-block;margin: 0 5px;"><a href=""><img src="https://dotbleu.alkurn.co.in/public/assets/front-end/images/instagram.png" alt="" style="width:35px;"></a></li>
    </ul>
</td>
</tr>
</table>
</td>
</tr>
    </tbody>
</table>

