<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Subscription;
use Hash;
use Auth;
use Session;
use DB;
use App\Notifications\sendPasswordLink;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
      return view('auth.login');
    }

    public function login(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:8'
      ]);

      if ($user = User::where('email', $request->email)->where('user_type', '!=', 'admin')->first()) 
      {
          if ($user->email_verified_at != null) 
          {
              if($user->password != '')
              {
                  $remember_me = $request->has('remember_me') ? true : false;
                  if (auth()->attempt(['email' => $request->email, 'password' => $request->password], $remember_me))
                  {
                      Auth::login($user);

                      if(Session::has('redirectURL'))
                      {
                          $redirect = Session::get('redirectURL');
                          Session::forget('redirectURL');
                      }
                      else
                          $redirect = "/";

                      if(Session::has('subscribeSubject'))
                      {
                        $subject = Session::get('subscribeSubject');
                        Session::forget('subscribeSubject');

                        if(Subscription::whereUserId(Auth::id())->whereSubjectId($subject)->where('from_date','<=',date('Y-m-d'))->where('to_date','>=',date('Y-m-d'))->first())
                        {
                          $redirect = 'channel-details/'.$subject;
                        }
                        else
                        {
                          $redirect = 'packages/'.$subject;
                        }
                      }
                      
                      return redirect($redirect);
                  }
                  else{
                    return back()->with('error','Invalid Login Attempt. If you’ve forgotten your password, please use the Forgot Password link.');
                  }
              }
              else
                {
                    return back()->with('error','This Email is registered with '.$user->provider.'. Please try login with that.');
                  }
          }
          else{
                $url = url('send-verification-mail/'.base64_encode($request->email));
                return back()->with('error','Your email is not verified. Please verify your email address and retry. <a href="'.$url.'">Click here</a> to resend the verification email. ');
          }
      }
      else
      {
        return redirect()->back()->with('error','Sorry, there is no record of an account associated with this email. Please retry.')->withInput($request->only('email', 'remember'));
      }
    }
    
    public function logout()
    {
        Auth::logout();
        return back();
    }

    public function forgetPassword(Request $request)
    {
        $request->validate(['email' => ['required', 'email']]);
        if ($user = User::where('email', $request->email)->where('user_type','!=','admin')->first()) {
            $token = str_random(60);
            $password_reset_user = DB::table('password_resets')
                ->where('email', $request->email)
                ->first();
            if ($password_reset_user) {
                $token_saved = DB::table('password_resets')
                    ->where('email', $password_reset_user->email)
                    ->update([
                        'token' => $token]);
            } else {
                $token_saved = DB::table('password_resets')->insert(['email' => $request->email,
                    'token' => $token, 'created_at' => date('Y-m-d H:i:s')]);
            }
            if ($token_saved) {
                $user->notify(new sendPasswordLink($user,$token));
                return back()->with('success', 'Please check your email for password reset instructions.');
            } else {
                return back()->with('error', 'This email does not exist.');
            }
        } else {
            return back()->with('error', 'This email does not exist.');
        }
    }

    public function updateForgotPassword(Request $request)
    {

        $validation = $this->validate($request, ['password' => 'required|min:8|confirmed', 'password_confirmation' => 'required']);
        $email = DB::table('password_resets')
            ->select('email')
            ->where('token', $request->token)
            ->first();
        $user = DB::table('users')
            ->select('*')
            ->where('email', $email->email)
            ->first();
        if ($request->password == $request->password_confirmation) {
            if ($user) {
                $password_updated = DB::table('users')
                    ->where('email', $user->email)
                    ->update(['password' => Hash::make($request->password)]);

                if ($password_updated) {
                    return redirect('/login')->with(['success' => 'Password was changed successfully.']);
                } else {
                    return redirect('/login')->with(['error' => 'There is an error while changing the password please try again later.!']);
                }
            }
        } else {
            return back()->with('error', 'Password do not matched with confirm password');
        }
    }

}
