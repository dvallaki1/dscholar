<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Notifications\registration;
use App\Notifications\adminUserRegistration;
use Auth;
use Socialite;
use Storage;
use App\Models\Country;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showSignupForm()
    {
        $countries = Country::where('country_status',1)->get();
        return view('auth.register',compact('countries'));
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ],[
            'email.unique'=>'The email is already registered on this site.',
            'password.confirmed'=>'The proposed passwords do not match.'
        ]);
    }

    protected function register(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],

        ], [
            "first_name.required" => "Please enter :attribute",
            "last_name.required" => "Please enter :attribute",
            "email.required" => "Please enter :attribute",
        ]);

        $formData['user_type'] = 'user';

        // if validation passes
        $password = $formData['password'];
        $formData['password'] = Hash::make($formData['password']);

        $formData['verify_token'] = str_random(30);

        // save the user to the database
        $user = User::create($formData);

        //Notify the user for the registration
        $user->notify(new registration($user));

        //Notify the admin for the registration
        $admin = User::whereUserType('admin')->first();
        $admin->notify(new adminUserRegistration ($user,$admin));

        // return a view 
        return redirect('/login')->with('success', 'Registration successful! Please check your email for verification.');
    }

     ### social login api
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider, Request $request)
    {
        if($request->error!='')
            return redirect('login');
        
        $userSocial = Socialite::driver($provider)->user();
        if($userSocial->email != null)
            $user = User::where('email', $userSocial->email)->first();
        else
            $user = User::where('provider_id', $userSocial->id)->first();

        if (!$user)
        {
            //create new user and login him
            $name = explode(' ', $userSocial->name);
            // $fileContents = file_get_contents($userSocial->getAvatar());
            // $filename = str_random(40);
            // Storage::put('profile_pics/'.$filename.'.jpg', $fileContents);

            $user = User::updateOrCreate(['first_name'=>$name[0], 'last_name'=>$name[1], 'email'=>$userSocial->email, 'email_verified_at'=>date('Y-m-d'), 'provider'=>$provider, 'provider_id'=>$userSocial->id, 'user_type'=>'user']);  

            //Notify the admin for the registration
            $admin = User::whereUserType('admin')->first();
            $admin->notify(new adminUserRegistration ($user,$admin));
                  
        }
        Auth::login($user);
        return redirect('/');
    }

    public function verify_user($token)
    {
        $verifyUser = User::where('verify_token', $token)->first();
        if($verifyUser){
            $verifyUser->email_verified_at = Date('Y-m-d H:i:s');
            $verifyUser->save();

            Auth::login($verifyUser);
        }
        return redirect('/');
        
    }

    public function sendVerificationMail($email)
    {
        $email = base64_decode($email);

        $user = User::whereEmail($email)->first();
        $user->verify_token = str_random(30);
        $user->save();

        //Notify the user for the registration
        $user->notify(new registration($user));

        return redirect('/login')->with('success', 'Verification email resent. Please check your email.');
    }
}
