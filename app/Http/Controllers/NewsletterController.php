<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Newsletter;
use Mail;
use RealRashid\SweetAlert\Facades\Alert;


class NewsletterController extends Controller
{
    public function subscribeNewsletter(Request $request)
    {
        $request->validate(['email' => 'required'],['email.required' => 'Please provide your email.'],['name' => 'required'],['name.required' => 'Please provide your name.']);

        if($request->name!='')
        {
            $name = explode(' ', $request->name);
            $fname = $name[0];
            if(isset($name[1]))
                $lname = $name[1];
            else
                $lname = '';
        }
        else
        {
            $fname = '';
            $lname = '';
        }

        if ( ! Newsletter::isSubscribed($request->email) ) {
            Newsletter::subscribeOrUpdate($request->email,['FNAME'=>$fname, 'LNAME'=>$lname]);
        }
        else
        {
            return back()->with('error', 'You have already subscribed for newsletter.');
        }
        $data = array('subscribe' => 1,'name'=>$request->name, 'email'=>$request->email, 'subscribe'=>1);

        Mail::send('emails.newsletter-subscribed', ['data'=>$data], function($message) use ($data){
            $message->to( $data['email']);
            $message->subject("Newsletter subscribed successfully");
        });

        return back()->with('success', 'You have successfully subscribed for newsletter.');

    }

    public function unsubscribeNewsletter($email)
    {
        $email = base64_decode($email);
        if ( Newsletter::isSubscribed($email) ) {
            Newsletter::unsubscribe($email);
        }
        $data = array('unsubscribe' => 1,'email'=>$email);

        Mail::send('emails.newsletter-subscribed', ['data'=>$data], function($message) use ($data){
            $message->to( $data['email']);
            $message->subject("Newsletter unsubscribed successfully");
        });

        return redirect('/')->with('success', 'You have successfully unsubscribed.');
    }
}
