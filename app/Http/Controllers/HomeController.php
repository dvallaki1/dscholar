<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CmsPage;
use App\Models\State;
use App\Models\City;
use App\Models\Faq;
use App\Models\User;
use App\Models\Blog;
use App\Models\Subject;
use App\Models\Contact;
use App\Models\Branch;
use App\Models\Video;
use App\Models\Subscription;
use App\Models\HomepageData;
use App\Models\HomepageUpcoming;
use App\Models\AboutpageData;
use App\Notifications\newContactEnquiry;
use Auth;

class HomeController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        $homepage = HomepageData::first();
        $homepage_upcomings = HomepageUpcoming::whereStatus(1)->get();
        return view('front-user.pages.home',compact('homepage','homepage_upcomings'));
    }

    public function about()
    {
        $about = AboutpageData::first();
        return view('front-user.pages.about',compact('about'));
    }

    public function cms($slug)
    {
        $page = CmsPage::whereSlug($slug)->first();

        if(!$page)
            \App::abort('404');

        else
            return view('front-user.pages.cms',compact('page'));
    }

    public function faqs()
    {
        $faqs = Faq::whereStatus(1)->get();
        return view('front-user.pages.faqs',compact('faqs'));
    }

    public function fetchStates(Request $request){
        $states = State::where('country_id', $request->country_id)->get();
        $data = '<option value="">Select State</option>';
        foreach ($states as $key => $state) {
            $data .= "<option value=$state->id>$state->state</option>";
        }
        return $data;
    }

    public function fetchCities(Request $request){
        $cities = City::where('state_id', $request->state_id)->get();
        $data = '<option value="">Select City</option>';
        foreach ($cities as $key => $city) {
            $data .= "<option value=$city->id>$city->city</option>";
        }
        return $data;
    }

    public function contact(Request $request)
    {
        $admin = User::whereUserType('admin')->first();

        $details = request()->except(['_token']);

        $contact = Contact::create($details);

        $admin->notify(new newContactEnquiry($admin, $details));

        return redirect()->back()->with('success', 'Your enquiry is submitted successfully. Admin will get back to you soon.');
    }

    public function blog(Request $request)
    {
        $blogs = Blog::whereStatus(1)->orderBy('created_at','desc')->paginate(4);

        foreach ($blogs as $blog) {
            $blog->content = $this->html_cut($blog->description,300);
        }

        if ($request->ajax()) {
            return \Response::json(\View::make('front-user.pages.blog-ajax', compact('blogs'))->render());
        }

        $latestBlogs = $this->getLatestBlogs($request);

        return view('front-user.pages.blog',compact('blogs','latestBlogs'));
    }

    function html_cut($text, $max_length)
    {
        $tags   = array();
        $result = "";

        $is_open   = false;
        $grab_open = false;
        $is_close  = false;
        $in_double_quotes = false;
        $in_single_quotes = false;
        $tag = "";

        $i = 0;
        $stripped = 0;

        $stripped_text = strip_tags($text);

        while ($i < strlen($text) && $stripped < strlen($stripped_text) && $stripped < $max_length)
        {
            $symbol  = $text[$i];
            $result .= $symbol;

            switch ($symbol)
            {
               case '<':
                    $is_open   = true;
                    $grab_open = true;
                    break;

               case '"':
                   if ($in_double_quotes)
                       $in_double_quotes = false;
                   else
                       $in_double_quotes = true;

                break;

                case "'":
                  if ($in_single_quotes)
                      $in_single_quotes = false;
                  else
                      $in_single_quotes = true;

                break;

                case '/':
                    if ($is_open && !$in_double_quotes && !$in_single_quotes)
                    {
                        $is_close  = true;
                        $is_open   = false;
                        $grab_open = false;
                    }

                    break;

                case ' ':
                    if ($is_open)
                        $grab_open = false;
                    else
                        $stripped++;

                    break;

                case '>':
                    if ($is_open)
                    {
                        $is_open   = false;
                        $grab_open = false;
                        array_push($tags, $tag);
                        $tag = "";
                    }
                    else if ($is_close)
                    {
                        $is_close = false;
                        array_pop($tags);
                        $tag = "";
                    }

                    break;

                default:
                    if ($grab_open || $is_close)
                        $tag .= $symbol;

                    if (!$is_open && !$is_close)
                        $stripped++;
            }

            $i++;
        }
        $result .= '...';
        
        while ($tags)
            $result .= "</".array_pop($tags).">";

        return $result;
    }

    public function getLatestBlogs(Request $request)
    {
        $condition = " 1 = 1 ";
        if(isset($request->keyword) && $request->keyword != '')
            $condition .= " and (title like '%".$request->keyword."%' or description like '%".$request->keyword."%')";

        $latestBlogs = Blog::whereRaw($condition)->whereStatus(1)->orderBy('view_count','desc')->take(3)->get();

        foreach ($latestBlogs as $blog) {
            $blog->content = $this->html_cut($blog->description,100);
        }

        if ($request->ajax()) {
            return \Response::json(\View::make('front-user.pages.latest-blog-ajax', compact('latestBlogs'))->render());
        }

        return $latestBlogs;

    }

    public function blogDetail($slug='', Request $request)
    {
        $blog = Blog::whereSlug($slug)->first();

        $latestBlogs = $this->getLatestBlogs($request);

        if(!$blog)
            App::abort('404');

        else
        {
            $blog->update(['view_count'=>$blog->view_count+1]);
            return view('front-user.pages.blog-detail',compact('blog','latestBlogs'));
        }
    }

    public function courses()
    {
        $branches = Branch::with('subjects')->whereStatus(1)->get();
        return view('front-user.pages.courses',compact('branches'));
    }

    public function videoChannels($id)
    {
        $subjects = Subject::whereBranchId($id)->get();
        return view('front-user.pages.video-channels',compact('subjects'));
    }

    public function channelDetails($id,Request $request)
    {
        $subject = Subject::with('videos')->find($id);
        $is_subscribed = Subscription::whereUserId(Auth::id())->whereSubjectId($id)->where('from_date','<=',date('Y-m-d'))->where('to_date','>=',date('Y-m-d'))->first(); 

        $videos = Video::whereSubjectId($id)->paginate(5);

        if ($request->ajax()) {
            
            //  return $request->all();
            return response()->json(view('front-user.pages.channel-details-ajax', compact('videos','is_subscribed'))->render());
        }
        return view('front-user.pages.channel-details',compact('subject','videos','is_subscribed'));
    }

}
