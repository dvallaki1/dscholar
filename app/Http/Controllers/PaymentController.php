<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Razorpay\Api\Api;
use Illuminate\Support\Str;
use App\Models\Subscription;
use App\Models\Subject;
use App\Models\User;
use App\Models\DiscountCodeUser;
use Auth;
use Session;
use App\Notifications\subscriptionSuccessfull;

class PaymentController extends Controller
{
    private $razorpayId;
    private $razorpayKey;

    public function __construct()
    {
        $this->razorpayId = env('RAZOR_PAY_KEY_ID');
        $this->razorpayKey = env('RAZOR_PAY_KEY_SECRET');
    }


    public function proceedToPay(Request $request)
    {
        // Generate random receipt id
        $receiptId = Str::random(20);
        
        // Create an object of razorpay
        $api = new Api($this->razorpayId, $this->razorpayKey);

        // In razorpay you have to convert rupees into paise we multiply by 100
        // Currency will be INR
        // Creating order
        $order = $api->order->create(array(
            'receipt' => $receiptId,
            'amount' => $request->all()['amount'] * 100,
            'currency' => 'INR'
            )
        );

        // Return response on payment page
        $response = [
            'orderId' => $order['id'],
            'razorpayId' => $this->razorpayId,
            'amount' => $request->all()['amount'] * 100,
            'name' => $request->all()['name'],
            'currency' => 'INR',
            'email' => $request->all()['email'],
            'contactNumber' => $request->all()['contactNumber'],
            'address' => 'Test Address',
            'description' => env('APP_NAME'),
        ];

        // Let's checkout payment page is it working
        //return view('payment-page',compact('response'));
        return view('front-user.pages.agree-to-pay',compact('response'));
    }

    public function Complete(Request $request)
    {
        // Now verify the signature is correct . We create the private function for verify the signature
        $signatureStatus = $this->SignatureVerify(
            $request->all()['rzp_signature'],
            $request->all()['rzp_paymentid'],
            $request->all()['rzp_orderid']
        );

        // If Signature status is true We will save the payment response in our database
        // In this tutorial we send the response to Success page if payment successfully made
        if($signatureStatus == true)
        {
            $subject_ids = Session::get('formData')['subject_ids'];
            foreach ($subject_ids as $subject) {
                $subjectDetail = Subject::find($subject);

                $subscription = Subscription::create(['subject_id'=>$subject,
                                        'user_id'=>Auth::id(),
                                        'from_date'=>date('Y-m-d'),
                                        'to_date'=>date("Y-m-d", strtotime("+".$subjectDetail->duration_in_months." months", strtotime(date('Y-m-d')) ))
                                    ]);
                $user = User::find(Auth::id());
                $user->notify(new subscriptionSuccessfull($user,$subjectDetail,$subscription));
            }

            //entry for user has used this discount code
            if($discount_id = Session::get('formData')['discount_id'])
                DiscountCodeUser::create(['user_id'=>Auth::id(),'discount_id'=>$discount_id]);

            Session::forget('formData');
            // You can create this page
            return redirect('/courses')->with('success','Subscription purchased successfully.');
        }
        else{
            // You can create this page
            return redirect('/courses')->with('error','Sorry, There is an error while subscribing. Please try again later.!');
        }
    }

    // In this function we return boolean if signature is correct
    private function SignatureVerify($_signature,$_paymentId,$_orderId)
    {
        try
        {
            // Create an object of razorpay class
            $api = new Api($this->razorpayId, $this->razorpayKey);
            $attributes  = array('razorpay_signature'  => $_signature,  'razorpay_payment_id'  => $_paymentId ,  'razorpay_order_id' => $_orderId);
            $order  = $api->utility->verifyPaymentSignature($attributes);
            return true;
        }
        catch(\Exception $e)
        {
            // If Signature is not correct its give a excetption so we use try catch
            return false;
        }
    }
}