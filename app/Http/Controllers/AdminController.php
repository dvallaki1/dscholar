<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Blog;
use App\Models\FreeDownload;
use App\Models\Faq;
use App\Models\CmsPage;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Subject;
use App\Models\Video;
use App\Models\Contact;
use App\Models\Subscription;
use App\Models\Branch;
use App\Models\Brochure;
use App\Models\HomepageData;
use App\Models\HomepageVideo;
use App\Models\HomepageUpcoming;
use App\Models\AboutpageData;
use App\Models\Email;
use App\Models\DiscountCode;
use App\Models\EmailContent;
use Hash;
use App\Notifications\addNewUser;
use DB;
use Auth;
use File;
use App\Mail\replyEnquiry;
use Mail;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

	public function index()
    {
        $count = new \stdClass();
        $count->users = User::whereUserType('user')->count(); 
        $count->doctors = User::whereUserType('doctor')->count();
        $recentUsers = User::orderBy('created_at','desc')->take(5)->get();
        $recentSubscriptions = Subscription::with('user')->with('subject')->orderBy('created_at','desc')->take(5)->get();
        return view('admin.dashboard',compact('count','recentUsers','recentSubscriptions'));
    }

    public function profile(){
         return view('admin.settings.profile');
    }

    public function updateAdminProfile(Request $request){

        $formData = request()->except(['_token']);

        if ($request->hasFile('profile_pic')) {
            $allowedfileExtension = ['jpeg', 'jpg', 'png', 'svg'];
            $file = $request->file('profile_pic');

            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedfileExtension)) {

                $formData['profile_pic'] = $file->store('profile_pics');
            }
            else
            {
                return back()->with('error', 'Please upload only jpg or png file.');
            }
        }
        
        $user = User::find(['id' => Auth::id()])->first();

        if ($user->update($formData)) {
            return back()->with('success','You have successfully updated the profile.!');

        } else {
            return back()->with('error','Sorry there is an error while updating your profile.!');

        }

    }

    public function changePassword(){
        return view('admin.settings.change-password');
    }

    public function  updateAdminPassword(Request $request)
    {
        $user = DB::table('users')
            ->select('*')
            ->where('id', Auth::guard('admin')->id())
            ->first();

        if (!Hash::check($request->oldpassword, $user->password)) {
            return back()->with('error', 'Sorry your current password does not match.!');
        }

        $this->validate($request, ['password' => 'required|confirmed|min:8']);

        if ($request->password == $request->password_confirmation) {
            if ($user) {
                $password_updated = DB::table('users')
                    ->where('id',  Auth::guard('admin')->id())
                    ->update(['password' => Hash::make($request->password)]);

                if ($password_updated) {
                    return back()->with(['success' => 'Password is changed successfully.!']);
                } else {
                    return back()->with(['error' => 'There is an error while changing the password please try again later.!']);
                }
            }
        } else {
            return back()->with('error', 'New password do not matched with confirm password.!');
        }
    }

    public function manageBlogs()
    {
        $result = Blog::all();
        return view('admin.blogs.manage', compact('result'));
    }

    public function addBlog()
    {
        return view('admin.blogs.add');
    }

    public function saveBlog(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
        ], [
            "title.required" => "Please enter :attribute",
            "description.required" => "Please enter :attribute",
        ]);

        if ($request->hasFile('image')) {
            $allowedfileExtension = ['jpeg', 'jpg', 'png', 'svg'];
            $file = $request->file('image');

            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedfileExtension)) {

                $formData['image'] = $file->store('blog_images');
            }
            else
            {
                return back()->with('error', 'Please upload only jpg or png file.');
            }
        }

        $formData['slug'] = str_slug($formData['title']);

        $blog = Blog::create($formData);

        if ($blog->save()) {
            return redirect('/admin/manage-blogs')->with('success', 'Blog added successfully.');
        } else {
            return redirect('/admin/manage-blogs')->with('fail', 'Sorry there is an error while adding blog. please try again.');
        }
    }

    public function editBlog($id)
    {
        $row = Blog::find($id);
        return view('admin.blogs.edit', compact('row'));
    }

    public function updateBlog($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
        ], [
            "title.required" => "Please enter :attribute",
            "description.required" => "Please enter :attribute",
        ]);

        if ($request->hasFile('image')) {
            $allowedfileExtension = ['jpeg', 'jpg', 'png', 'svg'];
            $file = $request->file('image');

            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedfileExtension)) {

                $formData['image'] = $file->store('blog_images');
            }
            else
            {
                return back()->with('error', 'Please upload only jpg or png file.');
            }
        }

        $formData['slug'] = str_slug($formData['title']);

        // save the row to the database
        $duplicateEntry = Blog::whereTitle($formData['title'])->first();

        if ($duplicateEntry == '' || ($duplicateEntry != '' && $duplicateEntry->id == $id)) {
            Blog::find($id)->update($formData);
        } else
            return redirect()->back()->with('error', 'Blog title already taken.');

        return redirect('/admin/manage-blogs')->with('success', 'Blog updated successfully.');
    }

    public function deleteBlog(Request $request)
    {
        $id = $request->input('id');

        if (Blog::find($id)->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted blog']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting blog. Please try again later!']);
        }
    }

    public function activateDeactivateBlog(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
            
        if($status == 1)
            $msg = 'You have successfully activated the blog.';
        else
            $msg = 'You have successfully deactivated the blog.';

        if (Blog::find($id)->update(['status'=>$status])) {
            return response()->json(['status' => 'success', 'msg' => $msg]);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error while changing the status of blog. Please try again later!']);
        }
    }

    public function managePages()
    {
        $result = CmsPage::all();
        return view('admin.pages.manage', compact('result'));
    }

    public function addPage()
    {
        return view('admin.pages.add');
    }

    public function savePage(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'title' => ['required', 'string', 'max:255'],
            'content' => ['required', 'string'],
        ], [
            "title.required" => "Please enter :attribute",
            "content.required" => "Please enter :attribute",
        ]);

        if ($request->hasFile('banner')) {
            $allowedfileExtension = ['jpeg', 'jpg', 'png', 'svg', 'JPG', 'PNG', 'JPEG', 'SVG'];
            $file = $request->file('banner');

            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedfileExtension)) {

                $formData['banner'] = $file->store('page_banners');
            }
            else
            {
                return back()->with('error', 'Please upload only jpg or png file.');
            }
        }

        $formData['slug'] = str_slug($formData['title']);
        //$formData['position'] = implode(',', $formData['position']);

        $page = CmsPage::create($formData);

        if ($page->save()) {
            return redirect('/admin/manage-pages')->with('success', 'Page added successfully.');
        } else {
            return redirect('/admin/manage-pages')->with('fail', 'Sorry there is an error while adding page. please try again.');
        }
    }

    public function editPage($id)
    {
        $row = CmsPage::find($id);
        return view('admin.pages.edit', compact('row'));
    }

    public function updatePage($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'title' => ['required', 'string', 'max:255'],
            'content' => ['required', 'string'],
        ], [
            "title.required" => "Please enter :attribute",
            "content.required" => "Please enter :attribute",
        ]);

        if ($request->hasFile('banner')) {
            $allowedfileExtension = ['jpeg', 'jpg', 'png', 'svg', 'JPG', 'PNG', 'JPEG', 'SVG'];
            $file = $request->file('banner');

            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedfileExtension)) {

                $formData['banner'] = $file->store('page_banners');
            }
            else
            {
                return back()->with('error', 'Please upload only jpg or png file.');
            }
        }

        $formData['slug'] = str_slug($formData['title']);
        //$formData['position'] = implode(',', $formData['position']);

        // save the row to the database
        $duplicateEntry = CmsPage::whereTitle($formData['title'])->first();

        if ($duplicateEntry == '' || ($duplicateEntry != '' && $duplicateEntry->id == $id)) {
            CmsPage::find($id)->update($formData);
        } else
            return redirect()->back()->with('error', 'Page title already taken.');

        return redirect('/admin/manage-pages')->with('success', 'Page updated successfully.');
    }

    public function deletePage(Request $request)
    {
        $id = $request->input('id');

        if (CmsPage::find($id)->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted page']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting page. Please try again later!']);
        }
    }

    public function activateDeactivatePage(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
            
        if($status == 1)
            $msg = 'You have successfully activated the page.';
        else
            $msg = 'You have successfully deactivated the page.';

        if (CmsPage::find($id)->update(['status'=>$status])) {
            return response()->json(['status' => 'success', 'msg' => $msg]);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error while changing the status of page. Please try again later!']);
        }
    }

    
    public function manageFaqs()
    {
        $result = Faq::all();
        return view('admin.faqs.manage', compact('result'));
    }

    public function addFaq()
    {
        return view('admin.faqs.add');
    }

    public function saveFaq(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'question' => ['required', 'string'],
            'answer' => ['required', 'string'],
        ], [
            "question.required" => "Please enter :attribute",
            "answer.required" => "Please enter :attribute",
        ]);

        $duplicateEntry = Faq::whereQuestion($formData['question'])->count();

        if ($duplicateEntry == 0) {
            $faq = Faq::create($formData);
            if ($faq->save()) {
                return redirect('/admin/manage-faqs')->with('success', 'FAQ added successfully.');
            } else {
                return redirect('/admin/manage-faqs')->with('fail', 'Sorry there is an error while adding FAQ please try again.');
            }
        } else {
            return redirect('/admin/manage-faqs')->with('success', 'Sorry this FAQ already exist.');
        }
    }

    public function editFaq($id)
    {
        $row = Faq::find($id);
        return view('admin.faqs.edit', compact('row'));
    }

    public function updateFaq($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'question' => ['required', 'string'],
            'answer' => ['required', 'string'],
        ], [
            "question.required" => "Please enter :attribute",
            "answer.required" => "Please enter :attribute",
        ]);

        // save the row to the database
        Faq::find($id)->update($formData);

        return redirect('/admin/manage-faqs')->with('success', 'FAQ updated successfully.');
    }

    public function deleteFaq(Request $request)
    {
        $id = $request->input('id');

        if (Faq::find($id)->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted FAQ']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting FAQ. Please try again later!']);
        }
    }

    public function activateDeactivateFaq(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
            
        if($status == 1)
            $msg = 'You have successfully activated the FAQ.';
        else
            $msg = 'You have successfully deactivated the FAQ.';

        if (Faq::find($id)->update(['status'=>$status])) {
            return response()->json(['status' => 'success', 'msg' => $msg]);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error while changing the status of FAQ. Please try again later!']);
        }
    }

    public function fetchStates(Request $request){
        $states = State::where('country_id', $request->country_id)->get();
        $data = '<option value="">Select State</option>';
        foreach ($states as $key => $state) {
            $data .= "<option value=$state->id>$state->state</option>";
        }
        return $data;
    }

    public function fetchCities(Request $request){
        $cities = City::where('state_id', $request->state_id)->get();
        $data = '<option value="">Select City</option>';
        foreach ($cities as $key => $city) {
            $data .= "<option value=$city->id>$city->city</option>";
        }
        return $data;
    }

    public function manageUsers()
    {
        $result = User::whereUserType('user')->get();
        return view('admin.users.manage', compact('result'));
    }

    public function addUser()
    {
        $countries = Country::where('country_status',1)->get();
        return view('admin.users.add',compact('countries'));
    }

    public function saveUser(Request $request)
    {
        $formData = request()->except(['_token']);

        $password = str_random(10);

        $this->validate($request, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],

        ], [
            "first_name.required" => "Please enter :attribute",
            "last_name.required" => "Please enter :attribute",
            "email.required" => "Please enter :attribute",
        ]);

        $formData['password'] = Hash::make($password);
        $formData['user_type'] = 'user';
        $formData['email_verified_at'] = date("Y-m-d");

        $user = User::create($formData);

        //Notify the user for the registration
        $user->notify(new addNewUser($user, $password));

        if ($user->save()) {
            return redirect('/admin/manage-users')->with('success', 'User added successfully.');
        } else {
            return redirect('/admin/manage-users')->with('fail', 'Sorry there is an error while adding user. please try again.');
        }
    }

    public function editUser($id)
    {
        $row = User::find($id);
        $countries = Country::where('country_status',1)->get();
        $states = State::where('country_id', $row->country_id)->get();
        $cities = City::where('state_id', $row->state_id)->get();
        return view('admin.users.edit', compact('row','countries','states','cities'));
    }

    public function updateUser($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],

        ], [
            "first_name.required" => "Please enter :attribute",
            "last_name.required" => "Please enter :attribute",
            "email.required" => "Please enter :attribute",
        ]);

        // save the row to the database
        $duplicateEntry = User::whereEmail($formData['email'])->first();

        if ($duplicateEntry == '' || ($duplicateEntry != '' && $duplicateEntry->id == $id)) {
            User::find($id)->update($formData);
        } else
            return redirect()->back()->with('error', 'Email already taken.');

        return redirect('/admin/manage-users')->with('success', 'User updated successfully.');
    }

    public function deleteUser(Request $request)
    {
        $id = $request->input('id');

        if (User::find($id)->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted user']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting user. Please try again later!']);
        }
    }

    public function notifications()
    {
        DB::table('notifications')->where('read_at', NULL)->where('notifiable_id', Auth::id())->update(['read_at' => Date('Y-m-d H:i:s')]);
        
        $result = DB::table('notifications')
                            ->where('notifiable_id', Auth::user()->id)
                            ->select('*', 'notifications.created_at as generated_at')
                            ->orderBy('notifications.created_at', 'DESC')
                            ->get();

        return view('admin.settings.notifications', compact('result'));
    }

    public function manageSubjects()
    {
        $result = Subject::all();
        return view('admin.subjects.manage', compact('result'));
    }

    public function addSubject()
    {
        $branches = Branch::all();
        return view('admin.subjects.add',compact('branches'));
    }

    public function saveSubject(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'subject' => ['required', 'string', 'max:255'],
            'price' => ['required'],
        ], [
            "subject.required" => "Please enter :attribute",
            "price.required" => "Please enter :attribute",
        ]);

        $duplicateEntry = Subject::whereSubject($formData['subject'])->first();

        if ($duplicateEntry == '') {
            $subject = Subject::create($formData);
            return redirect('/admin/manage-subjects')->with('success', 'Subject added successfully.');
        } else {
            return redirect('/admin/manage-subjects')->with('fail', 'Sorry there is an error while adding subject. please try again.');
        }   
    }

    public function editSubject($id)
    {
        $branches = Branch::all();
        $row = Subject::find($id);
        return view('admin.subjects.edit', compact('row','branches'));
    }

    public function updateSubject($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'subject' => ['required', 'string', 'max:255'],
            'price' => ['required'],
        ], [
            "subject.required" => "Please enter :attribute",
            "price.required" => "Please enter :attribute",
        ]);

        // save the row to the database
        $duplicateEntry = Subject::whereSubject($formData['subject'])->first();

        if ($duplicateEntry == '' || ($duplicateEntry != '' && $duplicateEntry->id == $id)) {
            Subject::find($id)->update($formData);
        } else
            return redirect()->back()->with('error', 'Subject already exist.');

        return redirect('/admin/manage-subjects')->with('success', 'Subject updated successfully.');
    }

    public function deleteSubject(Request $request)
    {
        $id = $request->input('id');

        if (Subject::find($id)->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted subject']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting subject. Please try again later!']);
        }
    }

    public function activateDeactivateSubject(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
            
        if($status == 1)
            $msg = 'You have successfully activated the subject.';
        else
            $msg = 'You have successfully deactivated the subject.';

        if (Subject::find($id)->update(['status'=>$status])) {
            return response()->json(['status' => 'success', 'msg' => $msg]);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error while changing the status of subject. Please try again later!']);
        }
    }

    public function manageVideos()
    {
        $result = Video::with('subject')->get();
        return view('admin.videos.manage', compact('result'));
    }

    public function addVideo()
    {
        $subjects = Subject::all();
        return view('admin.videos.add',compact('subjects'));
    }

    public function saveVideo(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'title' => ['required', 'string', 'max:255']
        ], [
            "title.required" => "Please enter :attribute"
        ]);

        if ($request->hasFile('path')) {
            $file = $request->file('path');

            $formData['path'] = $file->store('videos');
        }

        $duplicateEntry = Video::whereTitle($formData['title'])->first();

        if ($duplicateEntry == '') {
            if ($request->hasFile('thumbnail')) {
                $allowedfileExtension = ['jpeg', 'jpg', 'png'];
                $file = $request->file('thumbnail');

                $extension = $file->getClientOriginalExtension();

                if (in_array($extension, $allowedfileExtension)) {

                    $formData['thumbnail'] = $file->store('video_thumbnails');
                }
                else
                {
                    return back()->with('error', 'Please upload only jpg or png file.');
                }
            }
            
            $video = Video::create($formData);
            return redirect('/admin/manage-videos')->with('success', 'Video added successfully.');
        } else {
            return redirect('/admin/manage-videos')->with('fail', 'Sorry there is an error while adding video. please try again.');
        }
    }

    public function editVideo($id)
    {
        $subjects = Subject::all();
        $row = Video::find($id);
        return view('admin.videos.edit', compact('row','subjects'));
    }

    public function updateVideo($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'title' => ['required', 'string', 'max:255']
        ], [
            "title.required" => "Please enter :attribute"
        ]);

        if ($request->hasFile('path')) {
            $file = $request->file('path');

            $formData['path'] = $file->store('videos');
        }
        // save the row to the database
        $duplicateEntry = Video::whereTitle($formData['title'])->first();

        if ($duplicateEntry == '' || ($duplicateEntry != '' && $duplicateEntry->id == $id)) {
            if ($request->hasFile('thumbnail')) {
                $allowedfileExtension = ['jpeg', 'jpg', 'png'];
                $file = $request->file('thumbnail');

                $extension = $file->getClientOriginalExtension();

                if (in_array($extension, $allowedfileExtension)) {

                    $formData['thumbnail'] = $file->store('video_thumbnails');
                }
                else
                {
                    return back()->with('error', 'Please upload only jpg or png file.');
                }
            }
            Video::find($id)->update($formData);
        } else
            return redirect()->back()->with('error', 'Video title already taken.');

        return redirect('/admin/manage-videos')->with('success', 'Video updated successfully.');
    }

    public function deleteVideo(Request $request)
    {
        $id = $request->input('id');

        if (Video::find($id)->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted video']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting video. Please try again later!']);
        }
    }

    public function activateDeactivateVideo(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
            
        if($status == 1)
            $msg = 'You have successfully activated the video.';
        else
            $msg = 'You have successfully deactivated the video.';

        if (Video::find($id)->update(['status'=>$status])) {
            return response()->json(['status' => 'success', 'msg' => $msg]);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error while changing the status of video. Please try again later!']);
        }
    }

    public function manageFreeDownloads()
    {
        $result = FreeDownload::all();
        return view('admin.free-downloads.manage', compact('result'));
    }

    public function addFreeDownload()
    {
        return view('admin.free-downloads.add');
    }

    public function saveFreeDownload(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'title' => ['required', 'string'],
        ], [
            "title.required" => "Please enter :attribute",
        ]);

        $duplicateEntry = FreeDownload::whereTitle($formData['title'])->count();

        if ($duplicateEntry == 0) {
            if ($request->hasFile('thumbnail')) {
                $allowedfileExtension = ['jpeg', 'jpg', 'png'];
                $file = $request->file('thumbnail');

                $extension = $file->getClientOriginalExtension();

                if (in_array($extension, $allowedfileExtension)) {

                    $formData['thumbnail'] = $file->store('free_download_thumbnail');
                }
                else
                {
                    return back()->with('error', 'Please upload only jpg or png file.');
                }
            }

            if ($request->hasFile('file')) {
                $allowedfileExtension = ['pdf'];
                $file = $request->file('file');

                $extension = $file->getClientOriginalExtension();

                if (in_array($extension, $allowedfileExtension)) {

                    $formData['file'] = $file->store('free_download_files');
                }
                else
                {
                    return back()->with('error', 'Please upload only pdf file.');
                }
            }

            $free_download = FreeDownload::create($formData);
            if ($free_download->save()) {
                return redirect('/admin/manage-free-downloads')->with('success', 'Free Download added successfully.');
            } else {
                return redirect('/admin/manage-free-downloads')->with('fail', 'Sorry there is an error while adding Free Download please try again.');
            }
        } else {
            return redirect('/admin/manage-free-downloads')->with('success', 'Sorry this Free Download already exist.');
        }
    }

    public function editFreeDownload($id)
    {
        $row = FreeDownload::find($id);
        return view('admin.free-downloads.edit', compact('row'));
    }

    public function updateFreeDownload($id, Request $request)
    {
        $formData = request()->except(['_token']);

        if ($request->hasFile('thumbnail')) {
            $allowedfileExtension = ['jpeg', 'jpg', 'png'];
            $file = $request->file('thumbnail');

            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedfileExtension)) {

                $formData['thumbnail'] = $file->store('free_download_thumbnail');
            }
            else
            {
                return back()->with('error', 'Please upload only jpg or png file.');
            }
        }

        if ($request->hasFile('file')) {
            $allowedfileExtension = ['pdf'];
            $file = $request->file('file');

            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedfileExtension)) {

                $formData['file'] = $file->store('free_download_files');
            }
            else
            {
                return back()->with('error', 'Please upload only pdf file.');
            }
        }
        FreeDownload::find($id)->update($formData);
        return redirect('/admin/manage-free-downloads')->with('success', 'Free Download updated successfully.');
        
    }

    public function deleteFreeDownload(Request $request)
    {
        $id = $request->input('id');

        if (FreeDownload::find($id)->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted Free Download']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting Free Download. Please try again later!']);
        }
    }

    public function manageSubscriptions($user_id)
    {
        $result = Subscription::with('subject')->with('user')->whereUserId($user_id)->get();
        return view('admin.subscriptions.manage', compact('result'));
    }

    public function manageEnquiries()
    {
        $result = Contact::all();
        return view('admin.enquiries.manage', compact('result'));
    }

    public function viewEnquiry($id)
    {
        $row = Contact::find($id);
        return view('admin.enquiries.view', compact('row'));
    }

    public function replyEnquiry($id, Request $request)
    {
        $reply = $request->reply;

        $contact = Contact::find($id);
        $contact->update(['is_replied'=>1]);

        Mail::to($contact->email)->send(new replyEnquiry($contact, $reply));

        return redirect('/admin/manage-enquiries')->with('success', 'You have successfully replied to contact enquiry.');
    }

    public function deleteEnquiry(Request $request)
    {
        $id = $request->input('id');

        if (Contact::find($id)->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted enquiry']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting enquiry. Please try again later!']);
        }
    }

    public function manageBranches()
    {
        $result = Branch::all();
        return view('admin.branches.manage', compact('result'));
    }

    public function addBranch()
    {
        return view('admin.branches.add');
    }

    public function saveBranch(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'branch' => ['required', 'string', 'max:255'],
        ], [
            "branch.required" => "Please enter :attribute",
        ]);

        $duplicateEntry = Branch::whereBranch($formData['branch'])->first();

        if ($duplicateEntry == '') {
            $branch = Branch::create($formData);
        } else
            return redirect()->back()->with('error', 'Branch already created.');

        return redirect('/admin/manage-branches')->with('success', 'Branch added successfully.');
    }

    public function editBranch($id)
    {
        $row = Branch::find($id);
        return view('admin.branches.edit', compact('row'));
    }

    public function updateBranch($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'branch' => ['required', 'string', 'max:255'],
        ], [
            "branch.required" => "Please enter :attribute",
        ]);

        // save the row to the database
        $duplicateEntry = Branch::whereBranch($formData['branch'])->first();

        if ($duplicateEntry == '' || ($duplicateEntry != '' && $duplicateEntry->id == $id)) {
            Branch::find($id)->update($formData);
        } else
            return redirect()->back()->with('error', 'Branch already created.');

        return redirect('/admin/manage-branches')->with('success', 'Branch updated successfully.');
    }

    public function deleteBranch(Request $request)
    {
        $id = $request->input('id');

        if (Branch::find($id)->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted branch']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting branch. Please try again later!']);
        }
    }

    public function activateDeactivateBranch(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
            
        if($status == 1)
            $msg = 'You have successfully activated the branch.';
        else
            $msg = 'You have successfully deactivated the branch.';

        if (Branch::find($id)->update(['status'=>$status])) {
            return response()->json(['status' => 'success', 'msg' => $msg]);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error while changing the status of branch. Please try again later!']);
        }
    }

    public function editBrochure()
    {
        $row = Brochure::first();
        return view('admin.cms.edit-brochure', compact('row'));
    }

    public function updateBrochure(Request $request)
    {
        $formData = request()->except(['_token']);

        if ($request->hasFile('brochure')) {
            $allowedfileExtension = ['pdf'];
            $file = $request->file('brochure');

            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedfileExtension)) {

                $formData['brochure'] = $file->store('brochures');
            }
            else
            {
                return back()->with('error', 'Please upload only pdf file.');
            }
        }
        Brochure::first()->update($formData);
        return back()->with('success', 'Brochure updated successfully.');
        
    }

    public function editHomepageData()
    {
        $row = HomepageData::first();
        return view('admin.cms.edit-homepage', compact('row'));
    }

    public function updateHomepageData(Request $request)
    {
        $formData = request()->except(['_token']);

        if ($request->hasFile('about_image')) {
            $allowedfileExtension = ['jpeg', 'jpg', 'png', 'svg'];
            $file = $request->file('about_image');

            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedfileExtension)) {

                $formData['about_image'] = $file->store('cms_images');
            }
            else
            {
                return back()->with('error', 'Please upload only jpg or png file.');
            }
        }

        HomepageData::first()->update($formData);
        return back()->with('success', 'Homepage Data updated successfully.');
        
    }

    public function manageHomepageVideos()
    {
        $result = HomepageVideo::all();
        return view('admin.homepage-videos.manage', compact('result'));
    }

    public function addHomepageVideo()
    {
        return view('admin.homepage-videos.add');
    }

    public function saveHomepageVideo(Request $request)
    {
        $formData = request()->except(['_token']);

        if ($request->hasFile('video')) {
            $allowedfileExtension = ['mp4','avi','flv','mpeg'];
            $file = $request->file('video');

            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedfileExtension)) {

                $formData['video'] = $file->store('homepage_videos');
            }
            else
            {
                return back()->with('error', 'Please upload only mp4/flv/avi/mpeg file.');
            }
        }

        $homepage_video = HomepageVideo::create($formData);
        if ($homepage_video->save()) {
            return redirect('/admin/manage-homepage-videos')->with('success', 'Homepage Video added successfully.');
        } else {
            return redirect('/admin/manage-homepage-videos')->with('fail', 'Sorry there is an error while adding Homepage Video please try again.');
        }
    }

    public function editHomepageVideo($id)
    {
        $row = HomepageVideo::find($id);
        return view('admin.homepage-videos.edit', compact('row'));
    }

    public function updateHomepageVideo($id, Request $request)
    {
        $formData = request()->except(['_token']);

        if ($request->hasFile('video')) {
            $allowedfileExtension = ['pdf'];
            $file = $request->file('video');

            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedfileExtension)) {

                $formData['video'] = $file->store('homepage_videos');
            }
            else
            {
                return back()->with('error', 'Please upload only mp4/flv/avi/mpeg file.');
            }
        }
        HomepageVideo::find($id)->update($formData);
        return redirect('/admin/manage-homepage-videos')->with('success', 'Homepage Video updated successfully.');
        
    }

    public function deleteHomepageVideo(Request $request)
    {
        $id = $request->input('id');

        if (HomepageVideo::find($id)->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted Homepage Video']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting Homepage Video. Please try again later!']);
        }
    }

    public function manageHomepageUpcomings()
    {
        $result = HomepageUpcoming::all();
        return view('admin.homepage-upcomings.manage', compact('result'));
    }

    public function addHomepageUpcoming()
    {
        return view('admin.homepage-upcomings.add');
    }

    public function saveHomepageUpcoming(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
        ], [
            "title.required" => "Please enter :attribute",
            "description.required" => "Please enter :attribute",
        ]);

        if ($request->hasFile('image')) {
            $allowedfileExtension = ['jpeg', 'jpg', 'png', 'svg'];
            $file = $request->file('image');

            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedfileExtension)) {

                $formData['image'] = $file->store('cms_images');
            }
            else
            {
                return back()->with('error', 'Please upload only jpg or png file.');
            }
        }

        $homepageUpcoming = HomepageUpcoming::create($formData);

        if ($homepageUpcoming->save()) {
            return redirect('/admin/manage-homepage-upcomings')->with('success', 'Homepage Upcoming added successfully.');
        } else {
            return redirect('/admin/manage-homepage-upcomings')->with('fail', 'Sorry there is an error while adding homepage upcoming. please try again.');
        }
    }

    public function editHomepageUpcoming($id)
    {
        $row = HomepageUpcoming::find($id);
        return view('admin.homepage-upcomings.edit', compact('row'));
    }

    public function updateHomepageUpcoming($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
        ], [
            "title.required" => "Please enter :attribute",
            "description.required" => "Please enter :attribute",
        ]);

        if ($request->hasFile('image')) {
            $allowedfileExtension = ['jpeg', 'jpg', 'png', 'svg'];
            $file = $request->file('image');

            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedfileExtension)) {

                $formData['image'] = $file->store('homepage-upcoming_images');
            }
            else
            {
                return back()->with('error', 'Please upload only jpg or png file.');
            }
        }

        // save the row to the database
        $duplicateEntry = HomepageUpcoming::whereTitle($formData['title'])->first();

        if ($duplicateEntry == '' || ($duplicateEntry != '' && $duplicateEntry->id == $id)) {
            HomepageUpcoming::find($id)->update($formData);
        } else
            return redirect()->back()->with('error', 'Homepage Upcoming title already taken.');

        return redirect('/admin/manage-homepage-upcomings')->with('success', 'Homepage Upcoming updated successfully.');
    }

    public function deleteHomepageUpcoming(Request $request)
    {
        $id = $request->input('id');

        if (HomepageUpcoming::find($id)->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted homepage upcoming']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting homepage upcoming. Please try again later!']);
        }
    }

    public function activateDeactivateHomepageUpcoming(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
            
        if($status == 1)
            $msg = 'You have successfully activated the homepage upcoming.';
        else
            $msg = 'You have successfully deactivated the homepage upcoming.';

        if (HomepageUpcoming::find($id)->update(['status'=>$status])) {
            return response()->json(['status' => 'success', 'msg' => $msg]);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error while changing the status of homepage upcoming. Please try again later!']);
        }
    }

    public function editAboutpageData()
    {
        $row = AboutpageData::first();
        return view('admin.cms.edit-aboutpage', compact('row'));
    }

    public function updateAboutpageData(Request $request)
    {
        $formData = request()->except(['_token']);

        if ($request->hasFile('image')) {
            $allowedfileExtension = ['jpeg', 'jpg', 'png', 'svg'];
            $file = $request->file('image');

            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedfileExtension)) {

                $formData['image'] = $file->store('cms_images');
            }
            else
            {
                return back()->with('error', 'Please upload only jpg or png file.');
            }
        }

        AboutpageData::first()->update($formData);
        return back()->with('success', 'About page Data updated successfully.');
        
    }

    public function manageEmails()
    {
        $result = Email::orderBy('created_at', 'desc')->get();
        return view('admin.emails.manage', compact('result'));
    }

    public function addEmail()
    {
        $users = User::whereUserType('user')->get();
        return view('admin.emails.add', compact('users'));
    }

    public function saveEmail(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'subject' => ['required', 'string'],
            'body' => ['required']
        ], [
            "subject.required" => "Please enter :attribute",
            "body.required" => "Please select :attribute",
        ]);

        $users = User::whereIn('id', $formData['user_ids'])->get();
        $emails = [];

        foreach ($users as $user) {
            $emails[] = $user->email;
        }
        $data = ['subject' => $request->subject,'body'=>$request->body];

        \Mail::send('emails.newEmailFromAdmin', ['data'=>$data], function($message) use ($emails,$data)
        {
            $message->bcc($emails)->subject($data['subject']);
        });

        $formData['user_ids'] = implode(',', $formData['user_ids']);

        Email::create($formData);

        return redirect('/admin/manage-emails')->with('success', 'Email sent successfully.');

    }

    public function editEmail($id)
    {
        $row = Email::find($id);
        $row->users_arr = explode(',', $row->user_ids);

        $users = User::whereUserType('user')->get();

        return view('admin.emails.edit', compact('row', 'users'));
    }

    public function updateEmail($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'subject' => ['required', 'string'],
            'body' => ['required']
        ], [
            "subject.required" => "Please enter :attribute",
            "body.required" => "Please select :attribute",
        ]);

        $users = User::whereIn('id', $formData['user_ids'])->get();
        $emails = [];

        foreach ($users as $user) {
            $emails[] = $user->email;
        }
        $data = ['subject' => $request->subject,'body'=>$request->body];

        \Mail::send('emails.newEmailFromAdmin', ['data'=>$data], function($message) use ($emails,$data)
        {
            $message->bcc($emails)->subject($data['subject']);
        });

        $formData['user_ids'] = implode(',', $formData['user_ids']);

        Email::create($formData);

        return redirect('/admin/manage-emails')->with('success', 'Email sent successfully.');
    }

    public function deleteEmail(Request $request)
    {
        $id = $request->input('id');

        if (Email::find($id)->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted email']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting email. Please try again later!']);
        }
    }

    public function manageDiscountCodes()
    {
        $result = DiscountCode::all();
        return view('admin.discount-codes.manage', compact('result'));
    }

    public function addDiscountCode()
    {
        return view('admin.discount-codes.add');
    }

    public function saveDiscountCode(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'code' => ['required', 'string'],
        ], [
            "code.required" => "Please enter :attribute",
        ]);

        $duplicateEntry = DiscountCode::whereCode($formData['code'])->count();

        if ($duplicateEntry == 0) {
           
            $discount_code = DiscountCode::create($formData);
            if ($discount_code->save()) {
                return redirect('/admin/manage-discount-codes')->with('success', 'Discount Code added successfully.');
            } else {
                return redirect('/admin/manage-discount-codes')->with('fail', 'Sorry there is an error while adding Discount Code please try again.');
            }
        } else {
            return redirect('/admin/manage-discount-codes')->with('success', 'Sorry this Discount Code already exist.');
        }
    }

    public function editDiscountCode($id)
    {
        $row = DiscountCode::find($id);
        return view('admin.discount-codes.edit', compact('row'));
    }

    public function updateDiscountCode($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'code' => ['required', 'string'],
        ], [
            "code.required" => "Please enter :attribute",
        ]);

        DiscountCode::find($id)->update($formData);
        return redirect('/admin/manage-discount-codes')->with('success', 'Discount Code updated successfully.');
        
    }

    public function deleteDiscountCode(Request $request)
    {
        $id = $request->input('id');

        if (DiscountCode::find($id)->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted Discount Code']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting Discount Code. Please try again later!']);
        }
    }

    public function manageEmailContents()
    {
        $result = EmailContent::all();
        return view('admin.email-contents.manage', compact('result'));
    }

    public function editEmailContent($id)
    {
        $row = EmailContent::find($id);
        return view('admin.email-contents.edit', compact('row'));
    }

    public function updateEmailContent($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'subject' => ['required', 'string'],
            'body' => ['required', 'string'],
        ], [
            "subject.required" => "Please enter :attribute",
            "body.required" => "Please enter :attribute",
        ]);

        EmailContent::find($id)->update($formData);
        return redirect('/admin/manage-email-contents')->with('success', 'Email Content updated successfully.');
        
    }


    
}
