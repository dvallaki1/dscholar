<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\FreeDownload;
use App\Models\Video;
use App\Models\Subject;
use App\Models\Subscription;
use App\Models\DiscountCode;
use App\Models\DiscountCodeUser;
use Auth;
use Hash;
use File;
use Session;
use PDF;
use App\Notifications\sendCertificate;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function changePassword(request $request)
    {
        $user = User::find(Auth::id());

        if (!Hash::check($request->current_password, $user->password)) {
            return back()->with('error', 'Sorry your current password does not match.');
        }

        $this->validate($request, ['password' => 'required|confirmed|min:8']);

        if ($request->password == $request->password_confirmation) {
            if ($user) {
                $password_updated = $user->update(['password' => Hash::make($request->password)]);

                if ($password_updated) {
                    return back()->with(['success' => 'Password is changed successfully.']);
                } else {
                    return back()->with(['error' => 'There is an error while changing the password please try again later.!']);
                }
            }
        } else {
            return back()->with('error', 'New password do not matched with confirm password.');
        }
    }

    public function editProfile($value='')
    {
        $countries = Country::where('country_status',1)->get();
        $states = State::where('country_id', Auth::user()->country_id)->get();
        $cities = City::where('state_id', Auth::user()->state_id)->get();
        return view('front-user.account.my-profile',compact('countries','states','cities'));
    }

    public function updateProfile(Request $request)
    {
        $formData = request()->except(['_token']);

        if ($request->hasFile('profile_pic')) {
            $allowedfileExtension = ['jpeg', 'jpg', 'png', 'svg'];
            $file = $request->file('profile_pic');
            File::delete($formData['oldImageValue']);

            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedfileExtension)) {

                $formData['profile_pic'] = $file->store('profile_pics');
            }
        }

        User::find(Auth::id())->update($formData);

        return back()->with('success', 'Profile updated successfully.');

    }

    public function freeDownloads()
    {
        $freeDownloads = FreeDownload::all();
        return view('front-user.account.free-downloads',compact('freeDownloads'));
    }

    public function videoDetails($id)
    {
        $video = Video::with('subject')->find($id);
        return view('front-user.pages.video-details',compact('video'));
    }

    public function packages($id)
    {
        $subjects = Subject::all();
        $selectedSubject = Subject::find($id);
        return view('front-user.pages.packages',compact('subjects','selectedSubject'));
    }

    public function getSubjectDetails($id)
    {
        $selectedSubject = Subject::find($id);
        return response()->json(['status' => 'success', 'subject' => $selectedSubject]);
    }

    public function agreeToPay(Request $request)
    {
        $formData = request()->except(['_token']);
        Session::put('formData',$formData);
        return view('front-user.pages.agree-to-pay',compact('formData'));
    }

    public function mySubscriptions()
    {
        $subscriptions = Subscription::with('subject')->where('to_date','>=',date('Y-m-d'))->whereUserId(Auth::id())->get();
        $pastSubscriptions = Subscription::with('subject')->where('to_date','<=',date('Y-m-d'))->whereUserId(Auth::id())->get();
        
        return view('front-user.account.my-subscriptions',compact('subscriptions','pastSubscriptions'));
    }

    public function getCertificate()
    {
        $subscriptions = Subscription::with('subject')->whereUserId(Auth::id())->get();
        return view('front-user.account.get-certificate',compact('subscriptions'));
    }

    public function mailCertificate(Request $request)
    {
        $formData = request()->except(['_token']);

        $data['admin'] = User::whereUserType('admin')->first();
        $data['user'] = $user = User::find(Auth::id());
        $subscriptions = Subscription::with('subject')->whereIn('id',$formData['subscription_ids'])->get();

        foreach ($subscriptions as $subscription) {
            $data['sub_id'] = $sub_id = 'SUB-'.str_pad($subscription->id, 8, "0", STR_PAD_LEFT);

            $data['subscription'] = $subscription;

            $graphic = env('APP_NAME').'_Graphic_Certificate_'.$sub_id.'.pdf';
            $pdf = PDF::loadView('front-user.pages.graphic-certificate', $data)->setPaper('a4', 'landscape');
            $pdf->save(storage_path('app/graphic_certificates/'.$graphic));
            $data['graphic'] = 'graphic_certificates/'.$graphic;

            $formal = env('APP_NAME').'_Formal_Certificate_'.$sub_id.'.pdf';
            $pdf = PDF::loadView('front-user.pages.formal-certificate', $data);
            $pdf->save(storage_path('app/formal_certificates/'.$formal));
            $data['formal'] = 'formal_certificates/'.$formal;  

            $user->notify(new sendCertificate($user, $data));
        }

        return back()->with('success', 'Congratulations!!! Certificates for all your selected subscriptions have been mailed.'); 
    }

    public function getCodeDetails($code)
    {
        $codeDetails = DiscountCode::whereCode($code)
                                ->where(function ($query) {
                                    $query->where('from_date', '<=', now())
                                        ->orWhereNull('from_date');
                                })
                                ->where(function ($query) {
                                    $query->where('to_date', '>=', now())
                                        ->orWhereNull('to_date');
                                })->first();
        if($codeDetails)
        {
            if(DiscountCodeUser::whereUserId(Auth::id())->whereDiscountId($codeDetails->id)->exists())
                return response()->json(['status' => 'error', 'msg' => 'Sorry! You have already used this Discount Code.']);
            else
                return response()->json(['status' => 'success', 'codeDetails' => $codeDetails]);
        }
        else
            return response()->json(['status' => 'error', 'msg' => 'Sorry! Entered Discount Code is not valid.']);
    }

}
