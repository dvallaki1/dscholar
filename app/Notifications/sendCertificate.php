<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\EmailContent;

class sendCertificate extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $user;
    public $data;

    public function __construct($user,$data)
    {
        $this->user = $user;
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {        
        $content = EmailContent::whereName('sendCertificate')->first();

        $newMail = (new MailMessage)
            ->greeting('Hello, ' . ucfirst($this->user->first_name) . ' ' . ucfirst($this->user->last_name))
            ->subject(env('APP_NAME').' - '.str_replace("[SUBSCRIPTION ID]", $this->data['sub_id'], $content->subject))
            ->line($content->body);

            if(isset($this->data['formal']))
                $newMail->attach(storage_path().'/app/'.$this->data['formal'], [
                        'as' => basename($this->data['formal']),
                        'mime' => mime_content_type(storage_path().'/app/'.$this->data['formal']),
                    ]);

            if(isset($this->data['graphic']))
                $newMail->attach(storage_path().'/app/'.$this->data['graphic'], [
                        'as' => basename($this->data['graphic']),
                        'mime' => mime_content_type(storage_path().'/app/'.$this->data['graphic']),
                    ]);
            
            $newMail->line('Thank you for using our application!');

        return $newMail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            
        ];
    }
}
