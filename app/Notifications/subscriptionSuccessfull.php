<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\EmailContent;

class subscriptionSuccessfull extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $user;
    public $subjectDetail;

    public function __construct($user,$subjectDetail,$subscription)
    {
        $this->user = $user;
        $this->subjectDetail = $subjectDetail;
        $this->subscription = $subscription;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $content = EmailContent::whereName('subscriptionSuccessfull')->first();

        $body = str_replace("[SUBJECT]", $this->subjectDetail->subject, $content->body);
        $body = str_replace("[FROM DATE]", date('d-m-Y',strtotime($this->subscription->from_date)), $body);
        $body = str_replace("[TO DATE]", date('d-m-Y',strtotime($this->subscription->to_date)), $body);

        return (new MailMessage)
            ->greeting('Hello, ' . ucfirst($this->user->first_name) . ' ' . ucfirst($this->user->last_name))
            ->subject(env('APP_NAME').' - '.$content->subject)
            ->line($body)
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            
        ];
    }
}
