<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = ['branch','status'];

    public function subjects()
    {
        return $this->hasMany(Subject::class,'branch_id','id');
    }
}
