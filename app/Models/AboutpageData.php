<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutpageData extends Model
{
    protected $fillable = ['image','description','left_title','left_description','right_title','right_description'];
}
