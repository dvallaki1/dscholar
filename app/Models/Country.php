<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    protected $fillable = [
        'country'
    ];

    public function states()
    {
        return $this->hasMany('App\State');
    }
}
