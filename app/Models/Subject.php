<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = ['branch_id','subject','price','duration_in_months'];

    public function videos()
    {
        return $this->hasMany(Video::class,'subject_id','id');
    }

    public function branch()
    {
        return $this->hasOne(Branch::class,'id','branch_id');
    }
}
