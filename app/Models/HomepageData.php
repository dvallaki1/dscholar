<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomepageData extends Model
{
    protected $fillable = ['title','sub_title','description','about_title','about_image','about_description','slider_section_title'];
}
