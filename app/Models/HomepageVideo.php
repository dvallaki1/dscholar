<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomepageVideo extends Model
{
    protected $fillable = ['video'];
}
