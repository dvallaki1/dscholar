<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiscountCodeUser extends Model
{
    protected $fillable = ['discount_id','user_id'];
}
