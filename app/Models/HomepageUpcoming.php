<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomepageUpcoming extends Model
{
    protected $fillable = ['title','description','image','status'];
}
