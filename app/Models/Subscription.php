<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = ['user_id','subject_id','from_date','to_date'];

    public function subject()
    {
        return $this->hasOne(Subject::class,'id','subject_id');
    }

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
}
