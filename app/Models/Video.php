<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = ['subject_id','title','subtitle','duration','path','thumbnail'];

    public function subject()
    {
        return $this->hasOne(Subject::class,'id','subject_id');
    }
}
